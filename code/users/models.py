from django.contrib.auth.models import AbstractUser


class NewDealUser(AbstractUser):
    """ subclass user to make it extendable when needed later """

    class Meta:
        db_table = 'newdeal_users'
