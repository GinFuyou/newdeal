import {DataTreeNode, NewDealTreeGridComponent} from '../new-deal-tree-grid.component';

export abstract class SelectionStrategy {
  public grid: NewDealTreeGridComponent;
  public abstract onSelectionChanged(leaf: DataTreeNode, selected: boolean): void;

}
