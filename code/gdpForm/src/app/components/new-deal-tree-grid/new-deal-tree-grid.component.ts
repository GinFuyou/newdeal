import {Component, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {SelectionStrategy} from './selectionStrategy/selection-strategy';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'newDealGrid',
  templateUrl: './new-deal-tree-grid.component.html',
  styleUrls: ['./new-deal-tree-grid.component.css']
})
export class NewDealTreeGridComponent implements OnInit {

  @Input() selectable: boolean = true;
  private selectionStrategy: SelectionStrategy;
  @Input() data: Array<object> = [];
  outputTreeData: Array<DataTreeNode> = [];
  @Input() rowSelect: (val: string, checked: boolean) => void;

  @Input() columns: Array<Column> = [];
  @Input() configuration: Configuration = {};

  @ViewChildren(HTMLInputElement) inputs: QueryList<HTMLInputElement>;

  collapsed = new Array<string>();

  constructor(private sanitizer:DomSanitizer) {
  }

  ngOnInit() {
    this.outputTreeData = this.getTreeNodesSorted();
  }

  public setSelectionStrategy(strategy: SelectionStrategy): void {
    this.selectionStrategy = strategy;
    if (this.selectionStrategy) {
      this.selectionStrategy.grid = this;
    }
  }

  public getCellStyle(data: object, column: Column, index: number): object {
    const style = {};
    if (column.hasOwnProperty('width')) {
      style['width'] = column['width'];
      style['min-width'] = column['width'];
    }
    if (column.hasOwnProperty('textAlign')) {
      style['text-align'] = column['textAlign'];
      switch (column['textAlign']) {
        case 'center': {
          style['padding'] = '0 !important';
          break
        }
        case 'left': {
          style['padding-left'] = '5px !important';
          break
        }
        case 'right': {
          style['padding-right'] = '5px !important';
          break
        }
      }
    } else {
      style['text-align'] = 'left';
    }
    if (index === 0) {
      let paddingLeft = 5;
      if (data) {
        paddingLeft += this.calculateOffset(data[this.configuration.parentId], 0) * 10;
      }
      style['padding-left'] = paddingLeft + 'px';
    }
    return style;
  }

  public getRowClass(leaf: DataTreeNode): string {
    if (this.hasCollapsedAncestor(leaf.data)) {
      return 'row hidden';
    }
    let visible = 'row visible ';
    if (this.isTreeNodeSelected(leaf)) {
      visible += 'selected';
    }
    return visible;
  }

  public showHideRow(leaf: DataTreeNode) {
    if (this.collapsed.indexOf(leaf.data[this.configuration.id]) !== -1) {
      this.collapsed = this.collapsed.filter(e => e !== leaf.data[this.configuration.id]);
    } else {
      this.collapsed.push(leaf.data[this.configuration.id]);
    }
  }

  public getSelector(leaf: DataTreeNode): string {
    return leaf.data[this.configuration.id] + '_selector';
  }

  public getHeaderColumns(): Array<Column> {
    const headerColumns = new Array<Column>();
    const processedGroups = new Array<Column>();
    for (const column of this.columns) {
      if (processedGroups.indexOf(column) !== -1) {
        continue;
      }
      if (column.headerGroup !== undefined) {
        const sameGroupColumns = this.columns.filter(cc => column.headerGroup === cc.headerGroup && cc !== column);
        let newWidth = 'calc( ' + (column.width ? column.width : '');
        processedGroups.push(column);
        for (const sameGroupColumn of sameGroupColumns) {
          newWidth += sameGroupColumn.width ? (' + ' + sameGroupColumn.width) : '';
          processedGroups.push(sameGroupColumn);
        }
        newWidth += ' )';
        headerColumns.push({width: newWidth, header: column.header, textAlign: column.textAlign} as Column)

      } else {
        headerColumns.push(column);
      }
    }
    return headerColumns;
  }

  public itemSelected(leaf: DataTreeNode, event) {
    if (this.rowSelect) {
      this.rowSelect(leaf.data[this.configuration.id], event.target.checked);

      if (this.selectionStrategy) {
        this.selectionStrategy.onSelectionChanged(leaf, event.target.checked);
      }
    }
  }

  public isTreeNodeSelected(leaf: DataTreeNode): boolean {
    let i = document.getElementById(this.getSelector(leaf));
    return i && i['checked'];
  }

  public clickTreeNode(leaf: DataTreeNode): void {
    let i = document.getElementById(this.getSelector(leaf));
    if (i) {
      i.click();
    }
  }

  public getValue(leaf: DataTreeNode, column: Column): string | SafeHtml {

    if (column.valueFormatter !== undefined) {
      return  this.sanitizer.bypassSecurityTrustHtml(column.valueFormatter(leaf.data[column.property], leaf.data));
    }
    return leaf.data[column.property];
  }

  public showExpander(leaf: DataTreeNode) {
    return this.data.filter(e => e[this.configuration.parentId] === leaf.data[this.configuration.id]).length > 0;
  }

  private hasCollapsedAncestor(data: object): boolean {
    if (data[this.configuration.parentId]) {
      if (this.collapsed.indexOf(data[this.configuration.parentId]) !== -1) {
        return true;
      }
      return this.hasCollapsedAncestor(this.data.filter(e => e[this.configuration.id] === data[this.configuration.parentId])[0])
    }
    return false;

  }

  private calculateOffset(catId: string, i: number): number {
    const p = this.data.filter(e => e[this.configuration.id] === catId)[0]
    if (p) {
      return this.calculateOffset(p[this.configuration.parentId], i + 1)
    }
    else {
      return i;
    }
  }

  private getTreeNodesSorted(): DataTreeNode[] {
    const roots = this.data.filter(e => e[this.configuration.parentId] === undefined || e[this.configuration.parentId] === null).map((e) => {
      return {data: e, children: []} as DataTreeNode;
    });
    let result = [];
    for (let root of roots) {
      this.fillTree(root);
      result = result.concat(this.getFlatListOfTree(root));
    }
    return result;
  }

  private getFlatListOfTree(root: DataTreeNode): DataTreeNode[] {
    let result = [];
    result.push(root);
    for (let node of root.children) {
      result = result.concat(this.getFlatListOfTree(node));
    }
    return result;
  }

  private fillTree(root: DataTreeNode) {
    root.children =
      this.data.filter(e => e[this.configuration.parentId] === root.data[this.configuration.id]).map((e) => {
        return {data: e, children: [], parent: root} as DataTreeNode;
      });
    for (let node of root.children) {
      this.fillTree(node)
    }
  }

  getIcon(leaf: DataTreeNode): any {
    return 'fa '+ (this.collapsed.indexOf(leaf.data[this.configuration.id]) === -1 ? 'fa-chevron-circle-down' : 'fa-chevron-circle-right');
  }
}


export interface Column {
  property?: string, //property name
  header?: string, //property name
  headerGroup?: string, //value
  width?: string, //value
  textAlign?: string, //value
  valueFormatter?: (value: string, row: object) => string

}

export interface Configuration {
  id?: string;
  parentId?: string;
  selectable?: string;
}


export interface DataTreeNode {
  data: any;
  children: DataTreeNode[];
  parent?: DataTreeNode;
}
