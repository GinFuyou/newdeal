import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GdpFormComponent } from './gdp-form.component';
import { DataService } from './services/data/data-service';
import { HttpClientModule } from '@angular/common/http';
import { HighchartsChartModule } from 'highcharts-angular';
import { RemoteDataService } from './services/data/remote-data-service';
import { InMemoryDataService } from './services/data/in-memory-data-service';
import { NewDealTreeGridComponent } from './components/new-deal-tree-grid/new-deal-tree-grid.component';

@NgModule({
  declarations: [
    GdpFormComponent,
    NewDealTreeGridComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HighchartsChartModule
  ],
  providers: [
    {
      provide: DataService,
      useClass: InMemoryDataService //debug
      // useClass: RemoteDataService //prod
    }
  ],
  bootstrap: [GdpFormComponent]
})
export class AppModule {
}
