import {Observable} from 'rxjs';
import {Category, TokenResponse, Unit, YearDataResponse} from '../../data/interfaces';

export abstract class DataService {

  public abstract getUnits(): Observable<Unit[]>;

  public abstract getToken(unitId: string): Observable<TokenResponse>;

  public abstract getCategories(): Observable<Category[]>;

  public abstract getYears(token: string): Observable<number[]>;

  public abstract getDataByYear(year: number, unitId: string): Observable<YearDataResponse>;

  public abstract getSeriesByYear(token: string, categoryId: string): Observable<number[]>;

}
