import {DataService} from './data-service';
import {Observable, of} from 'rxjs';
import {CategoriesResponse, Category, TokenResponse, Unit, YearDataResponse, YearsResponse} from '../../data/interfaces';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class InMemoryDataService extends DataService {

  series: any = {
    cat1: [1500, 1600, 1700, 1800, 1750, 1700, 2725],
    cat2: [106, 107, 111, 133, 221, 767, 1766],
    cat9: [106, 107, 111, 133, 221, 767, 1766],
    cat3: [106, 107, 111, 133, 221, 767, 1766],
    cat4: [106, 107, 111, 133, 221, 767, 1766],
    cat5: [1060, 1070, 1110, 1330, 2210, 7670, 17660],
    cat7: [1060, 1070, 1110, 1330, 2210, 7670, 17660],
    cat8: [1060, 1070, 1110, 1330, 2210, 7670, 17660],
  };


  categoryDataByYear = {
    cat1: {value: 1500, percent: 100},
    cat2: {value: 106, percent: 50},
    cat9: {value: 106, percent: 50},
    cat3: {value: 106, percent: 50},
    cat4: {value: 106, percent: 50},
    cat5: {value: 106, percent: 50},
    cat7: {value: 106, percent: 50},
    cat8: {value: 106, percent: 50},
  };

  public  getToken(unitId: string): Observable<TokenResponse> {
    return of({token: '1'} as TokenResponse);
  }

  public getCategories(): Observable<Category[]> {
    return of([
      {
        id: 'cat1',
        displayName: 'Общий ВВП',
        parentId: undefined
      } as Category,
      {
        id: 'cat9',
        displayName: '9',
        parentId: 'cat2'
      } as Category,
      {
        id: 'cat2',
        displayName: '2',
        parentId: 'cat1'
      } as Category,
      {
        id: 'cat3',
        displayName: '3',
        parentId: 'cat2'
      } as Category,
      {
        id: 'cat4',
        displayName: '4',
        parentId: 'cat1'
      } as Category
      ,
      {
        id: 'cat5',
        displayName: '5',
        parentId: 'cat4'
      } as Category,
      {
        id: 'cat7',
        displayName: '7',
        parentId: 'cat5'
      } as Category
      ,
      {
        id: 'cat8',
        displayName: '8',
        parentId: 'cat3'
      } as Category

    ]);
  }

  public getYears(token: string): Observable<number[]> {
    return of([1750, 1800, 1850, 1900, 1950, 1999, 2050]);
  }

  public getDataByYear(year: number, unitId: string): Observable<YearDataResponse> {
    return of(this.categoryDataByYear as YearDataResponse);
  }

  public getSeriesByYear(token: string, categoryId: string): Observable<number[]> {
    return of(this.series[categoryId]);
  }

  getUnits(): Observable<Unit[]> {
    return of([{id: '000', displayName: 'test', isDefault: true} as Unit]);
  }
}

