import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import * as Highcharts from 'highcharts';
import {DataService} from './services/data/data-service';
import {Category, TokenResponse, Unit, YearData} from './data/interfaces';
import {HighchartsChartComponent} from 'highcharts-angular';
import {NewDealConsts} from './data/newDealConsts';
import {Column, Configuration, DataTreeNode, NewDealTreeGridComponent} from './components/new-deal-tree-grid/new-deal-tree-grid.component';
import {flatMap, map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {formatNumber} from '@angular/common';
import {SelectionStrategy} from './components/new-deal-tree-grid/selectionStrategy/selection-strategy';
import {faChevronCircleRight} from '@fortawesome/free-solid-svg-icons/faChevronCircleRight';
import {faCheck} from '@fortawesome/free-solid-svg-icons';
import {FaIconComponent, FaIconLibrary} from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'app-root',
  templateUrl: './gdp-form.component.html',
  styleUrls: ['./gdp-form.component.css']
})
export class GdpFormComponent implements AfterViewInit {

  @ViewChild('chart') chart: HighchartsChartComponent;
  @ViewChild('newDealGrid') grid: NewDealTreeGridComponent;

  highcharts = Highcharts;
  options: Highcharts.Options = {} as Highcharts.Options;

  units: Unit;
  token: any;
  years: number[] = [];
  categories: Category[] = [];
  categoriesHiererchy: Map<string, string> = new Map<string, string>();

  totalGDP: Category;

  categoriesLoaded = false;

  selectedPoints: any[] = [];


  columns: Array<Column> = [
    {
      property: 'displayName',
      header: 'ВВП по использованию или по расходам',
    } as Column
  ];

  configuration: Configuration = {
    id: 'id',
    parentId: 'parentId'
  };


  constructor(private dataService: DataService, private cdr: ChangeDetectorRef) {
  }

  ngAfterViewInit(): void {
    this.initChart();
    this.setXAxis();
    this.setYAxis();


    this.dataService.getUnits().pipe(flatMap((unitsResponse: Unit[]) => {
      this.units = unitsResponse[0];
      return this.dataService.getToken(this.units.id);
    })).subscribe(tokenResponse => {
      this.token = tokenResponse.token;
      this.dataService.getYears(this.token).subscribe(years => {
        this.years = years;
        this.setXAxis();
      }, () => {
        console.log('error loading years')
      });
      this.getCategories();
    });
    this.grid.setSelectionStrategy(new GDPCascadeDeselectionStrategy());
  }

  private getCategories(): void {

    this.dataService.getCategories().subscribe(categories => {
      this.categories = categories;
      if (this.categories.length > 0) {
        this.totalGDP = this.categories.find(e => e.parentId === undefined || e.parentId === null);
        this.totalGDP.parentId = undefined; // Это и так так, но таблица ломается если не указано явно
        this.categoriesLoaded = true;
        this.cdr.detectChanges();
        this.calculateParentHierarchy(this.categories, this.totalGDP.id);
        if (this.totalGDP) {
          this.dataService.getSeriesByYear(this.token, this.totalGDP.id).subscribe(series => {
            this.addSeriesForCategory(this.totalGDP, series, true);
          });
        }
      }
    });

  }

  private initChart(): void {
    this.options.chart = {type: 'areaspline'} as Highcharts.ChartOptions;
    this.options.title = {text: NewDealConsts.CHART_TITLE};
    this.options.subtitle = {text: NewDealConsts.CHART_SUBTITLE};
    this.options.series = [];
  }

  private addSeriesForCategory(category: Category, seriesData: number[], pointSelect = false) {
    const series = {
      id: category.id,
      name: category.displayName,
      data: seriesData,
      tooltip: {valueSuffix: ' ' + this.units.displayName},
      allowPointSelect: pointSelect
    } as Highcharts.SeriesOptionsType;

    if (pointSelect) {
      series.point = {
        events: {
          unselect: (event1) => {
            this.pointUnSelected(event1);
          },
          select: (event1 => {
            this.pointSelected(event1);
          })
        }
      };
    }
    this.options.series.push(series);
    this.chart.updateOrCreateChart();
  }

  private removeSeriesByCategoryId(categoryId: string) {
    this.options.series = this.options.series.filter(e => e.id !== categoryId);
    this.chart.updateOrCreateChart();
  }

  private setXAxis(): void {
    this.options.xAxis = {
      categories: this.years as Array<any>,
      title: {enabled: NewDealConsts.CHART_TITLE_XAXIS_ENABLED, text: NewDealConsts.CHART_TITLE_XAXIS, tickmarkPlacement: 'on'}
    }  as Highcharts.XAxisOptions;
  }

  private setYAxis(): void {
    this.options.yAxis = {
      title: {text: NewDealConsts.CHART_TITLE_YAXIS}
      , labels: {
        formatter: (axisPoint) => {
          return axisPoint ? (axisPoint.value / 1000 as any) : undefined;
        }
      },
    } as Highcharts.YAxisOptions;
  }

  getTableData(): any[] {
    return this.categories;
  }

  getTableColumns(): Column[] {
    return this.columns;
  }

  selectionChanged(id: string, checked: boolean) {
    if (checked) {
      if (!this.options.series.find(e => e.id === id)) {
        const cat = this.categories.find(e => e.id === id) as Category;
        this.dataService.getSeriesByYear(this.token, cat.id).subscribe(series => {
          this.addSeriesForCategory(cat, series);
        });
      }
    }
    else {
      this.removeSeriesByCategoryId(id);
    }

  }

  private pointSelected(selectEvent): void {
    const point = selectEvent.target.series.data[selectEvent.target.index];
    if (selectEvent.accumulate) {
      if (this.selectedPoints.length > 1) {
        this.selectedPoints[0].select(false, true);
      }
    } else {
      this.selectedPoints = [];
    }
    if (this.selectedPoints.indexOf(point) === -1) {
      this.selectedPoints.push(point);
    }
    this.selectedYearsUpdated();
  }

  private pointUnSelected(selectEvent): void {
    const point = selectEvent.target.series.data[selectEvent.target.index];
    this.selectedPoints = this.selectedPoints.filter(e => e.category !== point.category);
    this.selectedYearsUpdated();
  }

  private getSortedSelectedPoints(): any[] {
    return this.selectedPoints.sort((e, e2) => {
      return e.category > e2.category ? 1 : -1;
    });
  }

  private selectedYearsUpdated() {
    this.columns.splice(1, this.columns.length);
    const width = this.selectedPoints.length > 1 ? '70' : '70';
    let i = 0;

    for (const sp of this.getSortedSelectedPoints()) {

      this.columns.push({
        property: 'value' + sp.category,
        width: width + 'px',
        headerGroup: '' + i,
        header: sp.category + '',
        textAlign: 'center'
      } as Column);

      this.columns.push({
        property: 'percent' + sp.category,
        width: width + 'px',
        headerGroup: '' + i,
        header: sp.category + '',
        valueFormatter: this.getPercentValueFormatter(),
        textAlign: 'center'
      } as Column);

      i++;

      this.dataService.getDataByYear(sp.category, this.units.id).subscribe(e => {
        for (let c in e) {
          const id = c;
          const data = e[c] as YearData;
          const cat = this.categories.find(e => e.id === id);
          if (cat) {
            cat['percent' + sp.category] = data.percent;
            cat['value' + sp.category] = data.value;
          }
        }
      });
    }

    if (this.selectedPoints.length > 1) {
      this.columns[this.columns.length - 1].valueFormatter =
        this.valueFormatterConstructor(this.columns[this.columns.length - 1].property, this.columns[this.columns.length - 3].property);
    }
    this.cdr.detectChanges();
  }


  private valueFormatterConstructor(text: string, text2: string): (s: string, obj: object) => string {
    return (ss: string, obj: object) => {
      return this.getPercentValueFormatter2(text, text2)(ss, obj);
    };
  }

  private getPercentValueFormatter(): (s: string, obj: object) => string {
    return (ss: any, obj: object) => {
      return formatNumber(ss as number, 'en_US', '1.0-2') + '%';
    };
  }

  private getPercentValueFormatter2(property1, property2): (s: string, obj: object) => string {
    return (ss: any, obj: object) => {
      let rstring = formatNumber(ss as number, 'en_US', '1.0-2') + '%';
      if (obj[property1] < obj[property2]) {
        rstring += '&nbsp;<i style="color: red" class="fa fa-angle-down"></i>';
      }
      if (obj[property1] > obj[property2]) {
        rstring += '&nbsp;<i style="color: green" class="fa fa-angle-up"></i>';
      }
      return rstring;
    };
  }

  private calculateParentHierarchy(categories: Category[], rootId?: string, parentHierarchy?: string) {
    const root = categories.find(e => (e.id === rootId));
    this.categoriesHiererchy.set(root.id, parentHierarchy);
    if (this.categoriesHiererchy.size === categories.length) {
      return;
    }
    if (root) {
      const newParentHierarchy = (parentHierarchy ? (parentHierarchy + '->') : '') + root.id;
      for (const category of categories.filter(e => e.parentId === rootId)) {
        this.calculateParentHierarchy(categories, category.id, newParentHierarchy);
      }
    }
  }

  private getSelectionStrategy(): SelectionStrategy {
    return new GDPCascadeDeselectionStrategy();
  }

}

export class GDPCascadeDeselectionStrategy extends SelectionStrategy {
  onSelectionChanged(leaf: DataTreeNode, selected: boolean) {
    if (selected) {
      this.cascadeDeselect(leaf);
      if (leaf.children.length > 0) {
        this.cascadeDeselect(leaf, false);
      }
    }
  }

  public cascadeDeselect(leaf: DataTreeNode, deselectParents = true): void {
    if (deselectParents) {
      if (leaf.parent) {
        if (this.grid.isTreeNodeSelected(leaf.parent)) {
          this.grid.clickTreeNode(leaf.parent)
        }
        this.cascadeDeselect(leaf.parent);
      }
    } else {
      for (let child of leaf.children) {
        if (this.grid.isTreeNodeSelected(child)) {
          this.grid.clickTreeNode(child);
        }
        this.cascadeDeselect(child, false);
      }
    }
  }


}
