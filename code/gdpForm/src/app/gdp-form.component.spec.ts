import { TestBed, async } from '@angular/core/testing';
import { GdpFormComponent } from './gdp-form.component';

describe('GdpFormComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GdpFormComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(GdpFormComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

});
