import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BalanceComponent } from './balance/balance.component';

import { GdpFormComponent } from './gdpForm/gdp-form.component';
import { DataService } from './gdpForm/services/data/data-service';
import { HighchartsChartModule } from 'highcharts-angular';
import { RemoteDataService } from './gdpForm/services/data/remote-data-service';
import { InMemoryDataService } from './gdpForm/services/data/in-memory-data-service';
import { NewDealTreeGridComponent } from './gdpForm/new-deal-tree-grid/new-deal-tree-grid.component';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './notFound/notFound.component';

import { environment } from '../environments/environment';

// определение маршрутов
const appRoutes: Routes = [
  { path: 'sna/balance', component: BalanceComponent },
  { path: 'sna', component: GdpFormComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    BalanceComponent,
    GdpFormComponent,
    NewDealTreeGridComponent,
    NotFoundComponent
  ],
  providers: [
    {
      provide: DataService,
      useClass: environment.production ? RemoteDataService : InMemoryDataService
    }
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    HighchartsChartModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
