import { BalanceOperationBase } from './balance-operation'
import { BalanceElementService } from './balance-elements.service'
import { Injectable } from '@angular/core'



@Injectable({
    providedIn: 'root'
})
export class BalanceOperationService {
    private balanceElementService: BalanceElementService;
    constructor(balanceElementService: BalanceElementService) {
        this.balanceElementService = balanceElementService;
    }

    getBalanceOperations(): BalanceOperationBase[] {
        return [new PrivateSpending(this.balanceElementService),
        new GovernmentSpending(this.balanceElementService),
        new GovernmentTaxes(this.balanceElementService),
        new GovernmentDebtIssuance(this.balanceElementService),
        new ConsolidatedGovernmentSpending(this.balanceElementService),
        new GovernmentSpendIngnobonds(this.balanceElementService),
        new WithdrawCurrency(this.balanceElementService),
        new DepositCurrency(this.balanceElementService),
        new BankLoan(this.balanceElementService),
        new BankLoanInterest(this.balanceElementService),
        new BankLoanrePayment(this.balanceElementService),
        new BankLoanDefault(this.balanceElementService),
        new BondIssuance(this.balanceElementService),
        new BondInterest(this.balanceElementService),
        new BondRepayment(this.balanceElementService),
        new BondDefault(this.balanceElementService),
        new BankLoanseCuritize(this.balanceElementService),
        new QE(this.balanceElementService),
        new QEBanks(this.balanceElementService),
        new OMORaiseRates(this.balanceElementService),
        new OMOLowerRates(this.balanceElementService),
        new GovernmentDebtIssuanceTTL(this.balanceElementService),
        new TreasuryMovesTTL(this.balanceElementService)
        ];
    }

}


export class PrivateSpending extends BalanceOperationBase {
    balanceElementService: BalanceElementService;

    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;


    }

    value: string = 'privatespending';
    text: string = 'Изменить частные расходы';


    run(value: number): void {
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.recalculateAll();
    }


    reverse(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.recalculateAll();
    };

}


export class GovernmentSpending extends BalanceOperationBase {
    balanceElementService: BalanceElementService;

    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'governmentspending';
    text: string = 'Изменить государственные расходы';

    run(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'T.Deposits').value -= value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'T.Deposits').value += value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.recalculateAll();
    };

}

export class GovernmentTaxes extends BalanceOperationBase {
    balanceElementService: BalanceElementService;

    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'governmenttaxes';
    text: string = 'Изменить налоги';

    run(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'T.Deposits').value += value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'T.Deposits').value -= value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.recalculateAll();
    };

}


export class GovernmentDebtIssuance extends BalanceOperationBase {
    balanceElementService: BalanceElementService;

    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'governmentdebtissuance';
    text: string = 'Выпустить государственные займы';

    run(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'T.Deposits').value += value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Treasuries').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'T.Deposits').value -= value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Treasuries').value -= value;
        this.balanceElementService.recalculateAll();
    };

}

//consolidatedgovernmentspending
export class ConsolidatedGovernmentSpending extends BalanceOperationBase {
    balanceElementService: BalanceElementService;

    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'consolidatedgovernmentspending';
    text: string = 'Увеличить траты государства (консолидировано)';

    run(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Treasuries').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Treasuries').value -= value;
        this.balanceElementService.recalculateAll();
    };

}

export class GovernmentSpendIngnobonds extends BalanceOperationBase {
    balanceElementService: BalanceElementService;

    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'governmentspendingnobonds';
    text: string = 'Увеличить траты государства (без займов)';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.recalculateAll();
    };
}

export class WithdrawCurrency extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'withdrawcurrency';
    text: string = 'Клиент банка снимает валюту со счета';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Currency').value += value;
        this.balanceElementService.Banks.liabilities.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Currency').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Currency').value -= value;
        this.balanceElementService.Banks.liabilities.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Currency').value -= value;
        this.balanceElementService.recalculateAll();
    };
}

export class DepositCurrency extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'depositcurrency';
    text: string = 'Клиент банка пополняет счет валютой (обратное предыдущему)';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Currency').value += value;
        this.balanceElementService.Banks.liabilities.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Currency').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Currency').value -= value;
        this.balanceElementService.Banks.liabilities.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Currency').value += value;
        this.balanceElementService.recalculateAll();
    };
}

export class BankLoan extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'bankloan';
    text: string = 'Банковский кредит';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Loans').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Loans').value -= value;
        this.balanceElementService.recalculateAll();
    };
}

export class BankLoanInterest extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'bankloaninterest';
    text: string = 'Заемщик выплачивает проценты по банковскому кредиту';

    run(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.recalculateAll();
    };
}

export class BankLoanrePayment extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'bankloanrepayment';
    text: string = 'Заемщик выплачивает основную сумму из банковского кредита';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Loans').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Loans').value += value;
        this.balanceElementService.recalculateAll();
    };
}


export class BankLoanDefault extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'bankloandefault';
    text: string = 'Дефолт заемщика по банковскому кредиту';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Loans').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Loans').value += value;
        this.balanceElementService.recalculateAll();
    };
}

export class BondIssuance extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'bondissuance';
    text: string = 'Выпущены частные облигации';

    run(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Bonds').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Companies.liabilities.find(x => x.name == 'Bonds').value += value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Bonds').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Companies.liabilities.find(x => x.name == 'Bonds').value -= value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.recalculateAll();
    };
}
export class BondInterest extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'bondinterest';
    text: string = 'Выплаченные проценты по частным облигациям';

    run(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.recalculateAll();
    };
}

export class BondRepayment extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'bondrepayment';
    text: string = 'Погашение основной суммы частной облигации';

    run(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Bonds').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Companies.liabilities.find(x => x.name == 'Bonds').value -= value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Bonds').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Companies.liabilities.find(x => x.name == 'Bonds').value += value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.recalculateAll();
    };
}


export class BondDefault extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'bonddefault';
    text: string = 'Дефолт по частной облигации';

    run(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Bonds').value -= value;
        this.balanceElementService.Companies.liabilities.find(x => x.name == 'Bonds').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Bonds').value += value;
        this.balanceElementService.Companies.liabilities.find(x => x.name == 'Bonds').value += value;
        this.balanceElementService.recalculateAll();
    };
}


export class BankLoanseCuritize extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'bankloansecuritize';
    text: string = 'Банковский кредит секьюритизирован';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Loans').value -= value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Secur.Loans').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Loans').value += value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Companies.assets.find(x => x.name == 'Secur.Loans').value -= value;
        this.balanceElementService.recalculateAll();
    };
}

export class QE extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'qe';
    text: string = 'Количественное ослабление (вариант 1 - продают домохозяйства)';

    run(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Treasuries').value -= value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value += value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Households.assets.find(x => x.name == 'Treasuries').value += value;
        this.balanceElementService.Households.assets.find(x => x.name == 'Deposits').value -= value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value -= value;
        this.balanceElementService.recalculateAll();
    };
}

export class QEBanks extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'qebanks';
    text: string = 'Количественное ослабление (вариант 2 - продают банки)';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Treasuries').value -= value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Treasuries').value += value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value -= value;
        this.balanceElementService.recalculateAll();
    };
}

export class OMORaiseRates extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'omoraiserates';
    text: string = 'Операции на открытом рынке: повышение ставок до целевого уровня';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Treasuries').value += value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Treasuries').value -= value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value += value;
        this.balanceElementService.recalculateAll();
    };
}

export class OMOLowerRates extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'omolowerrates';
    text: string = 'Операции на открытом рынке: снижение ставок к цели';

    run(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Treasuries').value -= value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Banks.assets.find(x => x.name == 'Treasuries').value += value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value -= value;
        this.balanceElementService.recalculateAll();
    };
}

export class GovernmentDebtIssuanceTTL extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'governmentdebtissuancettl';
    text: string = 'Государственный долг (банки покупают через TT&L)';

    run(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'TTandL').value += value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Treasuries').value += value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'TTandL').value -= value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Treasuries').value -= value;
        this.balanceElementService.recalculateAll();
    };
}

export class TreasuryMovesTTL extends BalanceOperationBase {
    constructor(balanceElementService: BalanceElementService) {
        super(balanceElementService);
        this.balanceElementService = balanceElementService;
    }

    value: string = 'treasurymovesttl';
    text: string = 'Казначейство переводит средства из банка TT&L в центральный банк';

    run(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'TTandL').value -= value;
        this.balanceElementService.Treasury.assets.find(x => x.name == 'T.Deposits').value += value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value -= value;
        this.balanceElementService.Banks.liabilities.find(x => x.name == 'TTandL').value -= value;
        this.balanceElementService.recalculateAll();
    }

    reverse(value: number) {
        this.balanceElementService.Treasury.assets.find(x => x.name == 'TTandL').value += value;
        this.balanceElementService.Treasury.assets.find(x => x.name == 'T.Deposits').value -= value;
        this.balanceElementService.Banks.assets.find(x => x.name == 'Reserves').value += value;
        this.balanceElementService.Banks.liabilities.find(x => x.name == 'TTandL').value += value;
        this.balanceElementService.recalculateAll();
    };
}
