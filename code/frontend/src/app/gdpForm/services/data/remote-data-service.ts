import {DataService} from './data-service';
import {Observable} from 'rxjs';
import {CategoriesResponse, Category, TokenResponse, Unit, YearDataResponse, YearsResponse} from '../../data/interfaces';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class RemoteDataService extends DataService {

  baseUrl = '/sna/api/v1/gdp/';
  tokenUrl = this.baseUrl + 'token/';
  categoriesUrl = this.baseUrl + 'categories/';
  yearsUrl = this.baseUrl + 'years/';
  yearDataUrl = this.baseUrl + 'data/byYear/';
  unitsUrl = this.baseUrl + 'units';

  constructor(private httpClient: HttpClient) {
    super();
  }

  public getToken(unitId: string): Observable<TokenResponse> {
    return this.httpClient.get<TokenResponse>(this.tokenUrl + '?unitId=' + unitId);
  }

  public getCategories(): Observable<Category[]> {
    return this.httpClient.get<Category[]>(this.categoriesUrl);
  }

  public getYears(token: string): Observable<number[]> {
    return this.httpClient.get<number[]>(this.yearsUrl + token);
  }

  public getDataByYear(year: number, unitId: string): Observable<YearDataResponse> {
    return this.httpClient.get<YearDataResponse>(this.yearDataUrl + year + '?unitId='+ unitId);
  }

  public getSeriesByYear(token: string, categoryId: string): Observable<number[]> {
    return this.httpClient.get<number[]>(this.baseUrl + 'data/' + token + '/byCategory/' + categoryId + '/series');
  }

  getUnits(): Observable<Unit[]> {
    return this.httpClient.get<Unit[]>(this.unitsUrl);
  }
}
