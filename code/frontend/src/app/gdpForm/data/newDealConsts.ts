export class NewDealConsts {

  public static MONEY_UNIT_BIG = 'млрд. руб';
  public static MONEY_UNIT_SMALL = 'млн. руб';
  public static YEAR = 'Год';
  public static GDP = 'ВВП России';
  public static CHART_TITLE = NewDealConsts.GDP
  public static CHART_SUBTITLE = 'Источник: Росстат';
  public static CHART_TITLE_YAXIS = NewDealConsts.MONEY_UNIT_BIG;
  public static CHART_TITLE_XAXIS = NewDealConsts.YEAR;
  public static CHART_TITLE_XAXIS_ENABLED = false;


}
