export interface Category {
  id: string;
  parentId?: string;
  displayName: string;
}

export interface CategoryData {
  category: Category;
}

export interface CategoriesResponse {
  categories: Category[];
}


export interface Unit {
  id: string,
  displayName: string,
  isDefault: boolean
}

export interface TokenResponse {
  token: string;
}

export interface YearsResponse {
  year: number[];
}


export interface YearData {
  value: number;
  percent: number;
}

export interface YearDataResponse {
  [prop: string]: YearData;
}
