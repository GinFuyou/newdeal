#!/bin/bash
cd "$(dirname "${BASH_SOURCE[0]}")"
npm --prefix ./gdpForm install
npm run build --prefix ./gdpForm
echo Coping css...
cp ./gdpForm/dist/gdpForm/*.css ./sna/static/sna
echo Coping js
cp ./gdpForm/dist/gdpForm/*.js ./sna/static/sna
echo Coping txt
cp ./gdpForm/dist/gdpForm/*.txt ./sna/static/sna
echo Build finished
