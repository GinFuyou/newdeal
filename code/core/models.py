import bleach
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.functional import cached_property
from django.utils.html import strip_tags
from django.utils.translation import gettext_lazy as _
from home.model_base import AbstractDateTimeTrackedModel
from wagtail.admin.edit_handlers import FieldPanel  # , HelpPanel
from wagtail.admin.edit_handlers import MultiFieldPanel
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.core.fields import RichTextField
from wagtail.images import get_image_model_string
from wagtail.images.edit_handlers import ImageChooserPanel


class NewDealUser(AbstractUser):
    """ subclass user to make it extendable when needed later """

    class Meta:
        db_table = 'newdeal_users'


@register_setting
class NewDealSettings(BaseSetting):

    class Meta:
        verbose_name = _('base project settings')

    main_logo = models.ForeignKey(get_image_model_string(),
                                  blank=True,
                                  null=True,
                                  on_delete=models.SET_NULL,
                                  verbose_name=_('main logo'))

    fontawesome_kit = models.CharField(blank=True, default="f91e3d92b2", max_length=256)
    fontawesome_kit.help_text = _('e.g. f99e3b91c1, leave empty to disable')

    panels = [
        ImageChooserPanel('main_logo'),
        FieldPanel('fontawesome_kit', classname='full'),
    ]


'''
# NOTE decide to move make into indepnded models with own context processor
@register_setting
class ReferenceSettings(BaseSetting):
    """ Settings with links to social accounts and other sites """
    class Meta:
        verbose_name = _('project reference settings')

    links = models.ManyToManyField('RefLink', blank=True)

    panels = [
        FieldPanel('links', classname='full'),
    ]
'''


class RefType(models.Model):
    """ Reference resource type, hold base url, icon, title
        Use to define RefLink objects (see below)
    """
    class Meta:
        verbose_name = _('reference link type')

    name = models.CharField(_("reference name"), max_length=128, unique=True)
    base_url = models.URLField(_("base url"), max_length=128, unique=True)
    icon_code = models.TextField(_('icon code'), blank=True, max_length=2048)

    update_datetime = models.DateTimeField(verbose_name=_('date and time updated'),
                                           auto_now=True)

    panels = [
        FieldPanel('name', classname='full'),
        FieldPanel('base_url', classname='full'),
        FieldPanel('icon_code', classname='full'),
    ]

    def __str__(self):
        return self.name

    def clean(self):
        """ specific checks to ensure right format and safe html for icons """
        self.base_url = self.base_url.strip().lower()
        if self.base_url[:-1] != '/':
            self.base_url += '/'

        allowed_tags = ['b', 'i', 'svg',
                        'g', 'path',
                        'rect', 'circle',
                        'polygon', 'polyline'
                        'ellipse', 'defs',
                        'linearGradient',
                        'stop', 'text',
                        'img', 'span']

        self.icon_code = bleach.clean(self.icon_code, tags=allowed_tags)


class RefLinkQuerySet(models.QuerySet):
    def get_queryset(self):
        return super().get_queryset().select_related('reftype')

    def get_public(self):
        return self.filter(is_public=True).select_related('reftype')

    def get_for_footer(self):
        return self.get_public().filter(footer_position__gt=0).order_by('footer_position')


class RefLink(AbstractDateTimeTrackedModel):
    """ Reference link model for youtube, VK, donations, e.t.c.
        Resource is defined by reftype -> RefType
    """
    class Meta:
        verbose_name = _('reference link')
        unique_together = ('relative_url', 'reftype')

    relative_url = models.CharField(_("relative url"), max_length=1024)
    reftype = models.ForeignKey('RefType',
                                verbose_name=_('reference resource'),
                                on_delete=models.CASCADE)

    title = models.CharField(_('title'), max_length=92, blank=True)
    descr = models.TextField(_('description'), blank=True)
    label = models.CharField(_('label'), max_length=92, blank=True)
    label.help_text = _('set this text for first item in a group to label that group')

    is_public = models.BooleanField(_('is public'), default=True)
    is_source = models.BooleanField(_('is source for reposts'), default=False)
    footer_position = models.SmallIntegerField(_('position in footer'), default=20)
    footer_position.help_text = _("set to 0 to hide in footer")

    objects = RefLinkQuerySet.as_manager()

    panels = [
        FieldPanel('title', classname='full'),
        FieldPanel('reftype', classname='full'),
        FieldPanel('relative_url', classname='full'),
        MultiFieldPanel(
            [
                FieldPanel('is_public'),
                FieldPanel('is_source'),
                FieldPanel('footer_position'),
                FieldPanel('label'),
            ]
        )
    ]

    def __str__(self):
        name = f'[{self.reftype.name}]'
        if self.title:
            name += f' {self.title}'
        else:
            name += f' {self.relative_url}'
        return name

    @cached_property
    def url(self):
        return self.reftype.base_url + self.relative_url

    def clean(self):
        """ specific checks to ensure right formats """
        if self.relative_url.startswith(self.reftype.base_url):
            self.relative_url = self.relative_url.replace(self.reftype.base_url, '')
        else:
            base_url_without_schema = self.reftype.base_url.split('://')[-1]
            if self.relative_url.startswith(base_url_without_schema):
                self.relative_url = self.relative_url.replace(base_url_without_schema, '')


class SocialRepost(AbstractDateTimeTrackedModel):
    """ Messages reposted from outer sources like Telegram"""
    class Meta:
        verbose_name = _('social repost')

    reflink = models.ForeignKey(RefLink,
                                verbose_name=_('reference link of post source'),
                                limit_choices_to={'is_source': True},
                                on_delete=models.PROTECT)

    title = models.CharField(_('title'), max_length=128, blank=True)
    body = RichTextField(_('article body'))

    postlink = models.CharField(_('post link'), blank=True, max_length=1024)
    postlink.help_text = _('link within source resource to forwarded post if supported')

    forwarded_by = models.CharField(_('forwarded by'), blank=True, max_length=256)
    forwarded_by.help_text = _('name of user who reposted item to the site, if possible')

    is_public = models.BooleanField(default=True)

    panels = [
        FieldPanel('title', classname='full'),
        FieldPanel('body', classname='full'),
        FieldPanel('reflink', classname='full'),
        FieldPanel('postlink'),
        FieldPanel('forwarded_by'),
    ]

    def __str__(self):
        if self.title:
            return self.title
        else:
            return strip_tags(self.body[:40]) + "..."
