
from django.utils.html import mark_safe
from django.utils.translation import gettext_lazy as _
from wagtail.contrib.modeladmin.options import ModelAdmin as WagtailModelAmdin
from wagtail.contrib.modeladmin.options import (ModelAdminGroup,
                                                modeladmin_register)

from .models import RefLink, RefType, SocialRepost


class RefTypeAdmin(WagtailModelAmdin):
    model = RefType
    menu_label = RefType._meta.verbose_name_plural.title()
    menu_icon = "site"
    menu_order = 100
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ("name", "icon", "base_url", "update_datetime")
    search_fields = ("name", "base_url")

    def icon(self, obj):
        return mark_safe(
            f"<div class='socials__link'>{obj.icon_code}</div>"
        )


class RefLinkAdmin(WagtailModelAmdin):
    model = RefLink
    menu_label = RefLink._meta.verbose_name_plural.title()
    menu_icon = "link"
    menu_order = 200
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ('relative_url', 'icon', "reftype", 'title',
                    'is_public', 'footer_position',
                    "create_datetime", "update_datetime")
    list_filter = ("reftype",)
    search_fields = ("title", "relative_url")

    def icon(self, obj):
        return mark_safe(
            f"<div class='socials__link'>{obj.reftype.icon_code}</div>"
        )


class SocialRepostAdmin(WagtailModelAmdin):
    model = SocialRepost
    menu_label = model._meta.verbose_name_plural.title()
    menu_icon = "openquote"
    menu_order = 300
    list_display = (str, 'reflink', 'title', 'postlink',
                    'is_public', 'forwarded_by',
                    "create_datetime", "update_datetime")


class ReferenceGroup(ModelAdminGroup):
    menu_label = _('References')
    menu_icon = 'folder-open-inverse'  # change as required
    menu_order = 600  # will put in 3rd place (000 being 1st, 100 2nd)
    items = (RefTypeAdmin, RefLinkAdmin, SocialRepostAdmin)


# When using a ModelAdminGroup class to group several ModelAdmin classes together,
# you only need to register the ModelAdminGroup class with Wagtail:
modeladmin_register(ReferenceGroup)
