import json
import os

from django.conf import settings
# from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand  # , CommandError
# from rest_framework.renderers import JSONRenderer

from articles.models import Article
from articles.serializers import ArticleSerializer


class Command(BaseCommand):
    help = 'creates testing-purposes instances'
    DATA_PATH = settings.DATA_PATH

    def add_arguments(self, parser):
        parser.add_argument('-d', '--dump', action='store_true',
                            help='dump data to console')
        parser.add_argument('--clear', action='store_true',
                            help='clear items before loading')

    def handle(self, *args, **options):
        self.stdout.write(repr(options))
        path = os.path.join(self.DATA_PATH, 'test')

        if options['dump']:

            os.makedirs(path, exist_ok=True)

            qs = Article.objects.all()
            serializer = ArticleSerializer(qs, many=True)
            # data = JSONRenderer().render(serializer.data)
            data = json.dumps(serializer.data)

            self.stdout.write("Articles: ")
            with open(os.path.join(path, 'articles.json'), 'w') as f:
                f.write(data)

            self.stdout.write('\nFinished dumping data')

        elif settings.DEBUG:
            if options['clear']:
                self.stdout.write("! clear Articles")
                Article.objects.all().delete()

            with open(os.path.join(path, 'articles.json'), 'r') as f:
                data = json.load(f)
            serializer = ArticleSerializer(data=data, many=True)
            if serializer.is_valid():
                serializer.save()
            else:
                self.stderr.write(f"! {serializer.errors}")

            self.stdout.write(' Finished creating testing model instances')
