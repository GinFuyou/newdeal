import os
import shutil

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.contrib.staticfiles import finders
from django.core.management.base import BaseCommand  # , CommandError
from django.db import transaction
from wagtail.core.models import Site
from wagtail.images import get_image_model

from articles.models import ArticleIndex
from core.models import NewDealSettings
from home.models import HomePage


class Command(BaseCommand):
    help = 'create a testing-purposes GDP instances'

    def add_arguments(self, parser):
        parser.add_argument('-p', '--passwrord', help='password for test user')
        parser.add_argument('-I', '--no_articleindices',
                            action="store_true",
                            help="don't create article indices")
        parser.add_argument('-M', '--no_media',
                            action="store_true",
                            help="don't load default media")

    def handle(self, *args, **options):
        self.stdout.write(repr(options))

        if settings.DEBUG:
            User = get_user_model()
            USERNAME = 'nd_admin'
            if User.objects.filter(username=USERNAME).exists():
                self.stdout.write(f' - user already exists')
            else:
                user = User(username='nd_admin', is_staff=True, is_superuser=True)
                user.set_password(options.get('password', 'NewDeal2020'))
                user.full_clean()
                user.save()
                self.stdout.write(f' - created user {user.username}')

        home_page = HomePage.objects.first()
        home_page.fix_tree()
        self.stdout.write(f" - loaded home page, path: '{home_page.path}'")

        if options['no_articleindices']:
            self.stdout.write(' ! skip indices creation')
        else:
            count_indices = ArticleIndex.objects.filter(slug__in=('news', 'articles')).count()
            if count_indices > 0:
                self.stdout.write(f' - {count_indices} article indices already exist, skip')
            else:
                with transaction.atomic():
                    news_index_content_type, created = ContentType.objects.get_or_create(
                        model='articleindex',
                        app_label='articles'
                    )

                    news_index = ArticleIndex(
                        title='Новости',
                        slug='news',
                        content_type=news_index_content_type,
                        live=True)
                    article_index = ArticleIndex(
                        title='Статьи',
                        slug='articles',
                        content_type=news_index_content_type,
                        live=True)

                    home_page.add_child(instance=news_index)

                    home_page.add_child(instance=article_index)
                    self.stdout.write(' - created article indices')

        if options['no_media']:
            self.stdout.write(' ! skip media')
        else:
            site = Site.objects.get(is_default_site=True)
            nd_settings = NewDealSettings.for_site(site)
            WagtailImage = get_image_model()
            if not nd_settings.main_logo:
                logo_path = finders.find('default/default_logo.png')
                if logo_path:
                    new_path = os.path.join(settings.MEDIA_ROOT, 'branding/')
                    os.makedirs(new_path, exist_ok=True)
                    new_path += 'NewDeal.png'
                    shutil.copy(logo_path, new_path)
                    img = WagtailImage(file=new_path, title='NewDeal logo')
                    img.full_clean()
                    img.save()
                    nd_settings.main_logo = img
                    nd_settings.save()
                else:
                    self.stdout.write(' !! default logo not found')

        self.stdout.write(' Finished creating initial Wagtail pages')
