from django import template

register = template.Library()


@register.simple_tag
def group_refs(refs_queryset):
    if len(refs_queryset) > 0:
        outer_list = []
        inner_list = [refs_queryset[0]]
        for reflink in refs_queryset[1:]:
            if reflink.label:
                outer_list.append(inner_list.copy())
                inner_list = [reflink]
            else:
                inner_list.append(reflink)
        outer_list.append(inner_list)
        return outer_list
    else:
        return (refs_queryset,)  # that is a tuple
