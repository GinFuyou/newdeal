from .models import RefLink


def reference(request):
    context = {'reference_links': RefLink.objects.get_for_footer()}
    return context
