#!/bin/bash
echo Build started
cd "$(dirname "${BASH_SOURCE[0]}")"
npm --prefix ./frontend install
npm run build --prefix ./frontend
echo Build finished
