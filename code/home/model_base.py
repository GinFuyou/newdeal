from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class AbstractDateTimeTrackedModel(models.Model):
    """ Very basic abstract model that adds update_datetime and
        create_datetime
    """
    class Meta:
        abstract = True

    update_datetime = models.DateTimeField(verbose_name=_('date and time updated'),
                                           auto_now=True)
    create_datetime = models.DateTimeField(verbose_name=_('date and time created'),
                                           default=timezone.now)
