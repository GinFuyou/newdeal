from django.db import models
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.core.fields import RichTextField
from wagtail.core.models import Page

from articles.models import Article


class HomePage(Page):
    body = RichTextField(blank=True)
    show_recent_articles = models.PositiveSmallIntegerField(default=4)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
        FieldPanel('show_recent_articles'),
    ]

    # def serve(self, request):
    #    return super().serve(request)

    def get_context(self, request, *args, **kwargs):
        total_count = self.show_recent_articles
        context = super().get_context(request, *args, **kwargs)
        article_qs = Article.objects.for_homepage()[:total_count]

        count = len(article_qs)
        if count > 1:
            top_article, *article_qs = article_qs
        elif count == 1:
            top_article = article_qs[0]
            article_qs = Article.objects.none()
        else:
            top_article = None
            article_qs = Article.objects.none()

        context['articles'] = article_qs
        context['top_article'] = top_article

        return context
