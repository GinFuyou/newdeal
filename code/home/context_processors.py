from django.conf import settings


def common(request):
    context = {'PROJECT_VERSION': settings.PROJECT_VERSION,}
    return context
