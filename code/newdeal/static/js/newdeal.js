'use strict';

(function () {

  let header = {
    hheight: 0,
    sticked: false,

    sticky(){
      header.hheight = $('header').height()

      if($(document).scrollTop() >  header.hheight && !header.sticked){
        header.hheight = $('header').height()
        $('.header').addClass('sticky')
        $('main').css(`margin-top`, header.hheight)
        header.sticked = true
      } else if($(document).scrollTop() <  header.hheight && header.sticked) {
        console.log(`hi`)
        header.hheight = $('header').height()
        $('.header').removeClass('sticky')
        $('main').css(`margin-top`, 0)
        header.sticked = false
      }
    },
    init(){

      $(document).bind('scroll', function(){
        header.sticky()
      })
    }
  };

  let hamburger = {
    visible: false,
    opened: false,
    checkState(){
        if($(window).width() > 640 && hamburger.visible){
        hamburger.visible = false
        hamburger.setState(hamburger.visible)
        } else if($(window).width() < 641 && !hamburger.visible) {
        hamburger.visible = true
        hamburger.setState(hamburger.visible)
        }
    },
    setState($visible){
        if($visible == true){
        $('.main-menu__hamburger').addClass('main-menu__hamburger_visible')
        hamburger.opened = false
        $('.main-menu').find('.main-menu__list').removeClass('main-menu__list_visible')
        } else {
        $('.main-menu__hamburger').removeClass('main-menu__hamburger_visible')
        hamburger.opened = false
        $('.main-menu').find('.main-menu__list').removeClass('main-menu__list_visible')
        }
    },
    customClick(){
        if(mainMenu.opened){
        $('.main-menu__hamburger').removeClass(`opened`)
        mainMenu.opened = false
        mainMenu.setState()
        } else {
        $('.main-menu__hamburger').addClass(`opened`)
        mainMenu.opened = true
        mainMenu.setState()
        }
    },
    init(){
        hamburger.setState()
        $('.main-menu__hamburger').bind('click', function(e){
        hamburger.customClick(e)
        })
    }
  }

  var mainMenu = {
    opened: false,
    setState: function setState($kill) {
      if ($kill) {
        mainMenu.opened = false;
      }

      if (mainMenu.opened) {
        $('.main-menu__hamburger').closest('.main-menu').find('.main-menu__list').addClass('main-menu__list_visible');
        $('body').addClass('frozen');
        $('.wrapper').addClass('faded');
      } else {
        $('.main-menu__hamburger').closest('.main-menu').find('.main-menu__list').removeClass('main-menu__list_visible');
        $('body').removeClass('frozen');
        $('.wrapper').removeClass('faded');
      }
    }
  };

  var telefeed = {
    init: function init() {
      $();
    }
  };

  header.init();
  hamburger.init();
  $(window).on('resize', function () {
    hamburger.checkState();
  });
  $(window).on('click', function (e) {
    if ($(window).width() < 641) {
      if ($(e.target).closest('.main-menu').length < 1) {
        mainMenu.setState('kill');
      }
    }
  });
})();
