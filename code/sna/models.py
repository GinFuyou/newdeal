# from decimal import Context as DecimalContext
import warnings
from decimal import Decimal

from closure_tree.models import Node as ClosureTreeNode
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import ExpressionWrapper, F
from django.db.models.functions import Cast
from django.utils.translation import gettext_lazy as _
from home.model_base import AbstractDateTimeTrackedModel

# AbstractDateTimeTrackedModel priovides create\update tracking fields:
# update_datetime
# create_datetime


# do not mess up the number of zeroes, use this constant
MILLION = 1000000
SNA_CATEGORY_REFERENCE = 'node_id'
DECIMAL_BASE = 21
DECIMAL_DIGITS = (DECIMAL_BASE + 2, 2)


class SNACategoryPathNode(models.Model):
    """ this model works through PG View
        https://schinckel.net/2016/01/23/adjacency-lists-in-django-with-postgres/
    """
    root = models.ForeignKey('SNACategory', related_name='+', on_delete=models.DO_NOTHING)
    node = models.OneToOneField('SNACategory',
                                related_name='path_node',
                                primary_key=True,
                                on_delete=models.DO_NOTHING)
    # fall back to this field if well use only ancestors + other local field for ordering
    # ancestors = ArrayField(base_field=models.IntegerField())

    # lineage is like "ancestors + self"
    lineage = ArrayField(base_field=models.IntegerField())

    level = models.SmallIntegerField()

    class Meta:
        app_label = 'sna'
        managed = False
        db_table = 'sna_category_path_view'


class AbstractYearlyRecord(models.Model):
    """ Used for YearEntryRecord and probably for import/export records if
        they become separate models
    """
    class Meta:
        abstract = True  # abstract models are not translated to DB tables

    year_entry = models.ForeignKey('StatYearEntry',
                                   related_name="%(class)ss",
                                   verbose_name=_('year entry'),
                                   on_delete=models.PROTECT)
    int_value = models.BigIntegerField(verbose_name=_('integer value'))

    update_datetime = models.DateTimeField(verbose_name=('updated at'), auto_now=True)


class BaseRecordCastingQuerySet(models.QuerySet):
    _records_prefix = ''

    def annotate_casted(self, power10=6, decimal_places=2):  # add currency conversion ???
        power10 = int(power10)  # remove if float values become expected
        if power10 <= 1:
            # < 1 is an invalid value, return integer
            if power10 < 1:
                warnings.warn(f"{self.__class__.__name__}: invalid power of ten {power10} < 1, assuming 1")
            exp = F(f'{self._records_prefix}int_value')
            decimal_places = 0  # ignore argument and return integer
        else:
            exp = ExpressionWrapper(
                F(f'{self._records_prefix}int_value') / float(10 ** power10),
                output_field=models.FloatField()
            )

        if decimal_places > 0:  # negative values means float, zero - integer or float (no Cast)
            # cast values to decimal
            max_digits = DECIMAL_BASE + decimal_places
            exp = Cast(exp, output_field=models.DecimalField(max_digits=max_digits,
                                                             decimal_places=decimal_places))
        self = self.annotate(value=exp)
        return self


class SNACategoryQuerySet(BaseRecordCastingQuerySet):
    _records_prefix = "gdp_records__"

    def get_gdp_root(self):
        return self.filter(parent__isnull=True).get(code="b.1*g")

    def api_values_by_year(self, year_entry, power10=6, decimal_places=2):
        self = self.select_related('gdp_records').filter(gdp_records__year_entry=year_entry)
        """
        expression = ExpressionWrapper(
            F('gdp_records__int_value') / float(MILLION),
            output_field=ReprDecimalField(max_digits=DECIMAL_DIGITS[0],
                                             decimal_places=DECIMAL_DIGITS[1])
        )
        expression = F('gdp_records__int_value') / float(MILLION)
        """
        self = self.annotate_casted(power10, decimal_places)

        self = self.annotate(percent=F('gdp_records__percent'))
        return self.values(SNA_CATEGORY_REFERENCE, 'value', 'percent')

    def api_values_by_category(self, category_id, years=None, power10=6, decimal_places=2):
        self = self.select_related('gdp_records').filter(node_id=category_id)
        if years:
            self = self.filter(gdp_records__year_entry__in=years)
        self = self.order_by("gdp_records__year_entry")
        self = self.annotate_casted(power10, decimal_places)
        self = self.annotate(year=F('gdp_records__year_entry__year'))
        return self.values('year', 'value')


class SNACategory(ClosureTreeNode, AbstractDateTimeTrackedModel):
    """ System of Nationals Accounts models
    """
    class Meta:
        unique_together = (('name_en', 'parent'), ('parent', 'rank'))
        db_table = 'sna_categories'
        verbose_name = _('SNA category')
        verbose_name_plural = _('SNA categories')

    # unused now
    def default_rank():
        max_rank = SNACategory.objects.aggregate(max_rank=models.Max('rank'))['max_rank']
        if not max_rank:
            max_rank = 10000
        return max_rank + 100

    name_en = models.CharField(verbose_name=_('name (EN)'), max_length=512)

    parent = models.ForeignKey('self',
                               verbose_name=_('parent category'),
                               blank=True,
                               null=True,
                               related_name='children',
                               on_delete=models.SET_NULL)

    code = models.CharField(verbose_name=_('sna code'),
                            unique=True,
                            max_length=255)

    rank = models.SmallIntegerField(default=default_rank,
                                    verbose_name=_('rank'),)

    objects = SNACategoryQuerySet.as_manager()

    def __str__(self):
        return self.name_en


class SNACategoryTranslation(AbstractDateTimeTrackedModel):
    """ Provides translations for SNACategory
    """
    class Meta:
        unique_together = (('lang', 'category'),)
        db_table = 'sna_category_translations'
        verbose_name = _('SNA category translation')
        verbose_name_plural = _('SNA category translations')

    class LangChoices(models.TextChoices):
        RU = 'ru-RU', _('russian')

    category = models.ForeignKey('SNACategory',
                                 verbose_name=_('SNA category'),
                                 related_name='translations',
                                 on_delete=models.CASCADE)
    lang = models.CharField(verbose_name=_('language'),
                            max_length=6,
                            choices=LangChoices.choices,
                            default=LangChoices.RU,
                            db_index=True)
    name = models.CharField(verbose_name=_('name'), max_length=512)

    def __str__(self):
        return f"{self.name} ({self.lang})"


class StatYearEntry(models.Model):
    """ Year entry, may have additional yearly information
        like currency recalculation coefficient
    """
    class Meta:
        db_table = 'sna_years'
        verbose_name = _('year')

    year = models.PositiveSmallIntegerField(verbose_name=_('year'), primary_key=True)

    def __str__(self):
        return str(self.year)


class RecordQuerySet(BaseRecordCastingQuerySet):
    """ queryset and manager for YearEntryRecord """
    default_tree_order = ('year_entry__year', 'category__path_node__lineage')

    def with_category_tree(self):
        qs = self.select_related('category', 'category__path_node', 'year_entry')
        qs = qs.order_by(*self.default_tree_order)
        return qs

    def million_values(self):
        # self = self.annotate(value_units=models.Value('millions'))
        expression = ExpressionWrapper(
            F('int_value') / float(MILLION),
            output_field=models.DecimalField(max_digits=DECIMAL_DIGITS[0],
                                             decimal_places=DECIMAL_DIGITS[1])
        )
        # expression = F('int_value')
        return self.annotate(value=expression)  # TODO use DecimalField


class YearEntryRecord(AbstractYearlyRecord):
    """ Record for given category by year entry
    """
    class Meta:
        unique_together = (('year_entry', 'category'),)
        db_table = 'sna_gdp_records'
        verbose_name = _('GDP Record')

    category = models.ForeignKey('SNACategory',
                                 null=False,
                                 verbose_name=_('SNA category'),
                                 related_name='gdp_records',
                                 on_delete=models.PROTECT)

    year_entry = models.ForeignKey('StatYearEntry',
                                   related_name="records_for_year",
                                   verbose_name=_('year entry'),
                                   on_delete=models.PROTECT)

    percent = models.DecimalField(verbose_name=_('percent of total value'),
                                  max_digits=6,
                                  decimal_places=3,
                                  default=Decimal(0.0))

    objects = RecordQuerySet.as_manager()

    def __str__(self):
        return f"{self.category.name_en} ({self.year_entry.year})"
