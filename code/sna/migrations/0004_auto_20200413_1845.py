# Generated by Django 3.0.5 on 2020-04-13 15:45

from decimal import Decimal
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sna', '0003_path_node'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='yearentryrecord',
            name='value',
        ),
        migrations.AddField(
            model_name='yearentryrecord',
            name='int_value',
            field=models.BigIntegerField(default=0, verbose_name='integer value'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='yearentryrecord',
            name='percent',
            field=models.DecimalField(decimal_places=3, default=Decimal('0'), max_digits=6, verbose_name='percent of total value'),
        ),
        migrations.AlterField(
            model_name='yearentryrecord',
            name='year_entry',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='records_for_year', to='sna.StatYearEntry', verbose_name='year entry'),
        ),
    ]
