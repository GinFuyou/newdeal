# Generated by Django 3.0.4 on 2020-04-03 14:08

import closure_tree.fields
import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import sna.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='snacategory_Closure',
            fields=[
                ('path', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), primary_key=True, serialize=False, size=None)),
                ('depth', models.IntegerField()),
            ],
            options={
                'verbose_name': 'ancestor-descendant relationship',
                'verbose_name_plural': 'ancestor-descendant relationships',
                'db_table': 'sna_categories_closure',
                'db_tablespace': '',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='SNACategory',
            fields=[
                ('node_id', models.AutoField(primary_key=True, serialize=False)),
                ('update_datetime', models.DateTimeField(auto_now=True, verbose_name='date and time updated')),
                ('create_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date and time created')),
                ('name_en', models.CharField(max_length=512, verbose_name='name (EN)')),
                ('code', models.CharField(max_length=255, unique=True, verbose_name='sna code')),
                ('rank', models.SmallIntegerField(default=sna.models.SNACategory.default_rank, verbose_name='rank')),
                ('descendants', closure_tree.fields.ClosureManyToManyField(related_name='ancestors', through='sna.snacategory_Closure', to='sna.SNACategory')),
                ('parent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='children', to='sna.SNACategory', verbose_name='parent category')),
            ],
            options={
                'verbose_name': 'SNA category',
                'verbose_name_plural': 'SNA categories',
                'db_table': 'sna_categories',
                'unique_together': {('parent', 'rank'), ('name_en', 'parent')},
            },
        ),
        migrations.CreateModel(
            name='StatYearEntry',
            fields=[
                ('year', models.PositiveSmallIntegerField(primary_key=True, serialize=False, verbose_name='year')),
            ],
            options={
                'verbose_name': 'year',
                'db_table': 'sna_years',
            },
        ),
        migrations.CreateModel(
            name='SNACategoryPathNode',
            fields=[
                ('node', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, related_name='path_node', serialize=False, to='sna.SNACategory')),
                ('lineage', django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), size=None)),
                ('level', models.SmallIntegerField()),
            ],
            options={
                'db_table': 'sna_category_path_view',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='YearEntryRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.BigIntegerField(verbose_name='value')),
                ('update_datetime', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='gdp_records', to='sna.SNACategory', verbose_name='SNA category')),
                ('year_entry', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='yearentryrecords', to='sna.StatYearEntry', verbose_name='year entry')),
            ],
            options={
                'verbose_name': 'GDP Record',
                'db_table': 'sna_gdp_records',
                'unique_together': {('year_entry', 'category')},
            },
        ),
        migrations.CreateModel(
            name='SNACategoryTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('update_datetime', models.DateTimeField(auto_now=True, verbose_name='date and time updated')),
                ('create_datetime', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date and time created')),
                ('lang', models.CharField(choices=[('ru-RU', 'russian')], db_index=True, default='ru-RU', max_length=6, verbose_name='language')),
                ('name', models.CharField(max_length=512, verbose_name='name')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='sna.SNACategory', verbose_name='SNA category')),
            ],
            options={
                'verbose_name': 'SNA category translation',
                'verbose_name_plural': 'SNA category translations',
                'db_table': 'sna_category_translations',
                'unique_together': {('lang', 'category')},
            },
        ),
    ]
