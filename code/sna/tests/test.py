from time import time

from django.core.management import call_command
from django.test import Client, TestCase
from django.urls import reverse
from sna.models import SNACategory, StatYearEntry, YearEntryRecord

# from sna import views


_SEPARATOR = "\n***"


class ApiTestCase(TestCase):
    """ Test the API for correct data structure TODO: test error handling """
    def setUp(self):
        """ call init and data load commands to setup test DB """
        call_command('newdeal_init')
        call_command('sna_test_jsonload', '-c', '-r', '-s')
        print(_SEPARATOR)
        print(f"[{self.__class__.__name__}] setUp complete\n")

        self.client = Client()
        self.counter = 0

    def _get_api_data(self,
                      pattern_name,
                      namespace="sna:",
                      expected_code=200,
                      expects_list=False,
                      query_string='',
                      **kwargs):
        """ get retrieve data from url by given pattern_name and kwargs """
        url = reverse(f'{namespace}{pattern_name}', **kwargs)
        if query_string:
            url += "?" + query_string
        self.counter += 1
        print(f"{self.counter}. check {url: <72} -> ", end='')
        t = -time()
        response = self.client.get(url)
        t += time()
        t *= 1000
        print(f'({response.status_code}) {t:.1f} ms.')
        self.assertEqual(expected_code, response.status_code)

        # get the data and do a basic check on it's type
        data = response.json()
        self.assertIsNotNone(data)
        print(f"data is of type '{type(data).__name__}'")
        if expects_list:
            self.assertTrue(isinstance(data, list))
        else:
            self.assertTrue(isinstance(data, dict))
        return data

    def _check_data_structure(self, data, sample, display=True):
        self.assertEqual(type(sample), type(data))
        if isinstance(sample, list):
            item_count = len(data)
            print(f"got {item_count} item{'' if item_count==1 else 's'}")
            self.assertTrue(item_count > 0)
            data = data[0]
            sample = sample[0]

        keyset = set(data.keys())
        reference_keyset = set(sample.keys())
        if display:
            print(" compare data keys:")
            print(f"SMPL: [{' | '.join(sorted(reference_keyset))}]")
            print(f"DATA: [{' | '.join(sorted(keyset))}]")
        self.assertEqual(keyset, reference_keyset)

    def test_basic_api(self):
        """ this will be a single test because it's dependeding on returned data """

        # 1. test get token
        data = self._get_api_data('api_get_token')
        token = data['token']
        print(f"Got token: '{token}")
        self.assertIsNotNone(token)
        print("done\n")

        # 2. test years (requires token)
        sample = [2014, 2015, 2016, 2017, 2018]
        data = self._get_api_data('api_years', kwargs={'token': token}, expects_list=True)
        for i in data:
            self.assertTrue(isinstance(i, int))
        number_of_years = len(data)
        year_list = data
        print("done\n")

        # 3. test categories
        sample = [{
            "id": "string",
            "parentId": "string",
            "displayName": "string"
        }]
        expects_list = isinstance(sample, list)
        data = self._get_api_data('api_categories', expects_list=expects_list)
        print(f'categories ids: {[i["id"] for i in data]}')
        self._check_data_structure(data, sample)
        print("done\n")

        categories_data = data.copy()

        # 4. test get_data_series_by_category
        category_id = categories_data[0]['id']
        data = self._get_api_data('api_data_series_by_category',
                                  expects_list=True,
                                  kwargs={'token': token, 'categoryId': category_id})

        for entry in categories_data:
            response = self.client.get(reverse('sna:api_data_series_by_category',
                                               kwargs={'token': token, 'categoryId': entry['id']}))
            self.assertEqual(200, response.status_code)
            self.assertEqual(len(response.json()), number_of_years)
        print("done\n")

        # 5. test data by year
        sample = {
            "additionalProp1": {
                "value": 0,
                "percent": 0
            },
            "additionalProp2": {
                "value": 0,
                "percent": 0
            },
        }
        data = self._get_api_data('api_data_by_year',
                                  query_string='unitId=RUBx6',
                                  kwargs={'year': year_list[0]})
        data_keys = data.keys()
        print(f"data keys: {data_keys}")
        self.assertEqual({i['id'] for i in categories_data}, set(data_keys))

        # get one random subdict from sample
        sub_sample = sample[list(sample.keys())[0]]
        display_keys = True
        for key in data_keys:
            self._check_data_structure(data[key], sub_sample, display=display_keys)
            display_keys = False
        print("done\n")

        # 6. check error handling
        print("try a bad request:")
        data = self._get_api_data('api_data_by_year',
                                  query_string='unitId=RUx9',
                                  expected_code=400,
                                  kwargs={'year': year_list[1]})
        print(" data: {data}")

        print("no data should return empty objects on multiple objects views")
        print(" removing objects from test DB")
        YearEntryRecord.objects.all().delete()
        StatYearEntry.objects.all().delete()
        SNACategory.objects.all().delete()
        data = self._get_api_data('api_categories', expects_list=True)
        print(f' categories (should be empty): {repr(data)}')
        self.assertEqual(data, [])

        response = self.client.get(reverse('sna:api_data_series_by_category',
                                           kwargs={'token': token, 'categoryId': 42}))
        self.assertEqual(200, response.status_code)
        print(f' data series for a random category (should be list of zeroes): {repr(response.json())}')
        self.assertEqual(response.json(), [0] * number_of_years)

        print("try a proper request, but with no data now: (now it expects 400, may change!)")
        data = self._get_api_data('api_data_by_year',
                                  expected_code=400,
                                  kwargs={'year': year_list[-1]})
        print("done\n")
