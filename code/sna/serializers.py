from collections import OrderedDict
from decimal import Decimal
from decimal import getcontext as decimal_getcontext

from django.db import transaction
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from transliterate import translit

from . import models as sna_models


# TODO: it was not used after all, delete if not required
"""
class CategoryTreeSerializer(serializers.ModelSerializer):
    parent = RecursiveField(allow_null=True)
    id = serializers.CharField(source='code')

    class Meta:
        model = sna_models.SNACategory
        fields = ('id', 'parent')
"""


class CategoryTranSerializer(serializers.ModelSerializer):

    class Meta:
        model = sna_models.SNACategoryTranslation
        fields = ('lang', 'name', 'category_id')
        extra_kwargs = {'category_id': {'required': False,},}


class BaseCategorySerializer(serializers.ModelSerializer):
    """ base flat serializer, extend it or inherit from it """
    class Meta:
        model = sna_models.SNACategory
        fields = [
            'id',
            'translations',
            'name_en',
        ]

    id = serializers.CharField(source='code')
    translations = CategoryTranSerializer(many=True, required=False)


class FileImportCategorySerializer(BaseCategorySerializer):
    """ Serializer with hacks to convert file data, don't use it in real API """
    # translations = CategoryTranSerializer(many=True, required=False)  # restore this field if needed

    class Meta(BaseCategorySerializer.Meta):
        fields = [
            'id',
            'children',
            'translations',
            'name_en',
            'ru-RU',
            'en-US'
        ]

        # WARNING it's a dirty solution for importing non-API-compilant data
        extra_kwargs = {
            'ru-RU': {'required': False, 'source': 'translations'},
            'en-US': {'required': False, 'source': 'name_en'},
            'name_en': {'required': False},
        }

    children = serializers.ListField(child=RecursiveField(allow_null=True),
                                     required=False)

    def to_internal_value(self, data):

        # coerce local names to translations list
        data['translations'] = []
        available_language_codes = sna_models.SNACategoryTranslation.LangChoices.values

        data_keys = data.keys()

        # if english name was not provided, try to use other lang with transliterate
        if not (('en-US' in data_keys) or ('name_en' in data_keys)):
            data['name_en'] = translit(data.get('ru-RU', ''), 'ru', reversed=True)

        # pop lang codes so they don't mess further
        for lang in available_language_codes:
            if lang in data_keys:
                data['translations'].append(
                    {'name': data.pop(lang),
                     'lang': lang,}
                )

        ret = super().to_internal_value(data)

        return ret

    def create(self, validated_data):
        """ will use update_or_create to update existing data """

        category, created = sna_models.SNACategory.objects.update_or_create(
            code=validated_data['code'],
            defaults={'name_en': validated_data['name_en'],},
        )

        # add translations if list not empty
        if validated_data['translations']:
            for trans_data in validated_data['translations']:
                trans, created = sna_models.SNACategoryTranslation.objects.update_or_create(
                    lang=trans_data['lang'],
                    category=category,
                    defaults={'name': trans_data['name']}
                )

        # recursively add children if in data
        if validated_data.get('children'):
            children_list = []
            for child_data in validated_data['children']:
                children_list.append(self.create(validated_data=child_data))
            category.children.set(children_list)

        return category

    def validate(self, data):
        """
        Check that start is before finish.
        """
        # if
        #    raise serializers.ValidationError("finish must occur after start")
        return data


class SimpleRecordsDictField(serializers.Field):

    def to_representation(self, value):
        """ expected value is reverse related manager """
        related_manager = value

        values_list = related_manager.annotate_casted().values_list('category__pk', 'value')

        # print(f" to repr: {repr(values_list)}")
        decimal_getcontext().prec = 16
        representation = {i[0]: Decimal(i[1]) for i in values_list}
        return representation

    def to_internal_value(self, data):
        """ data expected to be a dict-like object """
        cleaned_dict = OrderedDict()
        has_root = False

        for key, value in data.items():
            # get category or raise ValidationError. It requires query for every record, optimize later
            try:
                category = sna_models.SNACategory.objects.values('pk', 'parent').get(code=key)
            except sna_models.SNACategory.DoesNotExist:
                raise serializers.ValidationError('SNACategory with code "%s" does not exists' % key)

            cleaned_dict[category['pk']] = int(value * sna_models.MILLION)

            # move root category(ies) to head of the ordered dict
            if category['parent'] is None:
                # we allow only one root category to be present here
                if has_root:
                    raise serializers.ValidationError(
                        f"Data referes more than 1 root category, which is not supported due to"
                        f" how GDP total is selected. (Failing at code={key} pk={category['pk']})"
                    )
                else:
                    has_root = True  # flag on a first root category, so we can catch second
                    cleaned_dict.move_to_end(category['pk'], last=False)
        return cleaned_dict


class APIRecordsSerializer(serializers.ModelSerializer):
    """ """
    class Meta:
        model = sna_models.StatYearEntry
        fields = ['value', 'percent']

    value = serializers.DecimalField(max_digits=sna_models.DECIMAL_DIGITS[0],
                                     decimal_places=sna_models.DECIMAL_DIGITS[1],
                                     read_only=True,
                                     source='int_value')

    def to_representation(self, instance):
        return {'value': getattr(instance, 'value', Decimal(instance.value / sna_models.MILLION)),
                'percent': getattr(instance, 'percent')}


class APIDataByYearSerializer(serializers.ModelSerializer):
    """
        1
            value   86014204300000
            percent "100.000"
        2
            value   61389774100000
            percent "71.372"
    """

    class Meta:
        model = sna_models.StatYearEntry
        fields = ['records']

    records = APIRecordsSerializer(many=True, source='records_for_year', read_only=True)

    def to_representation(self, instance):
        power10 = self.context['power10']
        decimal_places = self.context.get('decimal_places', 2)
        values = sna_models.SNACategory.objects.api_values_by_year(
            instance, power10, decimal_places
        )
        representation = {}
        for value_dict in values:
            category_ref = value_dict.pop(sna_models.SNA_CATEGORY_REFERENCE)
            representation[category_ref] = value_dict
        return representation


class SimpleYearlyRecordsSerializer(serializers.ModelSerializer):
    """ This serializer handles records as simple {code: value} pairs.
        it also handles conversion from decimal millions to internal interger single units
    """

    class Meta:
        model = sna_models.StatYearEntry
        fields = ['year', 'data']
        extra_kwargs = {'year': {'validators': []}}  # we don't want unique check for update

    data = SimpleRecordsDictField(source='records_for_year')

    _gdp_root_category = None

    def validate_year(self, value):
        """ replacement for dropped validators (see above) """

        try:
            value = int(value)
        except ValueError as ex:
            raise serializers.ValidationError(f"Field 'year' must be an integer: {ex}")

        # sanity checks for year value
        if (value < 1) or (value > 2200):
            raise serializers.ValidationError(f"Field 'year' got unexpected value: {value}")
        return value

    def validate(self, data):
        # check if parent category node (GDP total) exists
        try:
            self._gdp_root_category = sna_models.SNACategory.objects.get_gdp_root()
        except sna_models.SNACategory.DoesNotExist:
            raise serializers.ValidationError(
                "GDP total category doesn't exists, create it first before loading any data"
            )
        except sna_models.SNACategory.MultipleObjectsReturned as ex:
            raise serializers.ValidationError(
                f"Programming error: SNACategory.objects.get_gdp_root() returned multiple objects -"
                f" either function requires an update or there is problem with categories entries! "
                f"({ex})"
            )

        return data

    def create(self, validated_data):
        """ will use update_or_create to update existing data """

        year_entry, created = sna_models.StatYearEntry.objects.get_or_create(year=validated_data['year'])

        # in a single transaction create all records, calculating percentage values if total GDP is present
        with transaction.atomic():
            # this line will fail if data didn't include total GDP. At this point it's intended
            gdp_int_total = validated_data['records_for_year'][self._gdp_root_category.pk]
            for category_pk, int_value in validated_data['records_for_year'].items():
                percent = ((int_value) / (gdp_int_total)) * 100

                record, created = sna_models.YearEntryRecord.objects.update_or_create(
                    category_id=category_pk,
                    year_entry=year_entry,
                    defaults={'int_value': int_value,
                              'percent': percent}
                )
        return year_entry
