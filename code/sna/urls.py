from django.urls import include, path

from .views import views

app_name = 'sna'

api_patterns = [
    path('gdp/units/', views.get_units, name='api_get_units'),
    path('gdp/token/', views.get_token, name='api_get_token'),
    path('gdp/categories/', views.get_categories_tree, name='api_categories'),
    path('gdp/years/<str:token>', views.get_years, name='api_years'),
    path('gdp/data/byYear/<int:year>', views.get_data_by_year, name='api_data_by_year'),
    path('gdp/data/<str:token>/byCategory/<int:categoryId>/series',
         views.get_data_series_by_category,
         name='api_data_series_by_category'),
]


urlpatterns = [
    path('', views.index, name='sna_index'),
    path('balance/', views.balance_sheet, name='balance_sheet'),  # TODO Replace with Wagtail page
    path('upload', views.upload_file),
    path('api/v1/', include(api_patterns))
]
