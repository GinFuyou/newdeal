# import json

import logging

from celestia.view_container import ViewContainer, register_view
from django.conf import settings
from django.db.models import CharField, F
from django.db.models.functions import Cast
from django.http import (HttpResponse, HttpResponseBadRequest,
                         HttpResponseNotAllowed, JsonResponse)
from django.template import loader
from django.views.generic import View

from .forms import UploadFileForm
from .models import SNACategory, StatYearEntry
from .serializers import APIDataByYearSerializer

logger = logging.getLogger(getattr(settings, 'BASE_LOGGER', 'django'))

DEFAULT_UNITS = "RUBx6"

views = ViewContainer()


class APIView(View):
    """ Very simple API class-based view returning JsonResponse
        Note that by default CBVs return not allowed if they don't define
        corresponding method (see get())
    """
    extra_data = None
    response_headers = None
    safe_json = True
    check_kwargs = False
    bad_kwarg_values = ('undefined', 'nan', 'null', 'false')
    error_response = {}
    error_on_empty_data = False

    def dispatch(self, request, *args, **kwargs):
        """ before serving handler, check if arguments are not
            'matching url pattern but special values'
            (undefined, NaN, etc.)
            empty values should be handled by url path converters
        """
        if self.check_kwargs:
            errors = {}
            for key in self.check_kwargs:
                val = self.kwargs.get(key, '')
                if str(val).lower() in self.bad_kwarg_values:
                    errors[key] = f"Value '{val}' is not expected"
            if errors:
                msg = '[Bad Request 400] Argument check failed'
                if request.META.get('HTTP_ACCEPT').startswith("application/json"):
                    return JsonResponse(data={'status': False,
                                              'msg': msg,
                                              'code': 'bad_arguments',
                                              'errors': errors},
                                        status=400)
                else:
                    msg += " - " + '; '.join(
                        ["'{0}': {1}".format(key, val) for key, val in errors.items()]
                    )
                return HttpResponseBadRequest(msg)

        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """ GET method handler """
        data = self.get_data()
        if self.error_on_empty_data and not data:
            response = JsonResponse(
                self.error_response, status=self.error_response.get('status_code', 400)
            )
        else:
            response = JsonResponse(data, safe=self.safe_json)
        return self.process_response(response)

    def get_data(self, **kwargs):
        """ returns data for the response """
        if self.extra_data is not None:
            kwargs.update(self.extra_data)
        return kwargs

    def process_response(self, response):
        """ set headers for response """
        if self.response_headers:
            for key, value in self.response_headers.items():
                response[key] = value
        return response


class CachingAPIView(APIView):
    """ Just add a fixed cache-control header to APIView """
    response_headers = {'Cache-Control': f"max-age={settings.SNA_API_CACHE_SECONDS}"}


@register_view(views)
def index(request):
    template = loader.get_template("sna/index.html")
    context = {}
    return HttpResponse(template.render(context, request))

# TODO: replace this view with Wagtail Page
@register_view(views)
def balance_sheet(request):
    template = loader.get_template("sna/base_angularapp.html")
    context = {}
    return HttpResponse(template.render(context, request))


@register_view(views)
def get_units(request):
    if request.method != "GET":
        return HttpResponseNotAllowed()
    return JsonResponse([{"id": DEFAULT_UNITS, "displayName": "млн. руб"}], safe=False)


@register_view(views)
def get_token(request):
    if request.method != "GET":
        return HttpResponseNotAllowed()
    unitId = request.GET.get('unitId', DEFAULT_UNITS)
    if unitId:
        year_set = StatYearEntry.objects.order_by("year").values_list("year", flat=True)
        years = ";".join(map(str, year_set))
        if years == "":
            token = unitId
        else:
            token = unitId + ";" + years
        return JsonResponse({"token": token}, safe=False)
    else:
        return HttpResponseBadRequest()


@register_view(views)
class GetCategoriesTreeView(CachingAPIView):
    safe_json = False

    def get_data(self, **kwargs):

        integer_ids = self.request.GET.get('int', False)
        if integer_ids:
            qs = SNACategory.objects.select_related("translations").annotate(
                id=F("node_id"),
                parentId=F('parent_id'),
                displayName=F("translations__name")
            )
        else:
            qs = SNACategory.objects.select_related("translations").annotate(
                id=Cast("node_id", output_field=CharField()),
                parentId=Cast('parent_id', output_field=CharField()),
                displayName=F("translations__name")
            )
        qs = qs.values('id', 'parentId', 'displayName')
        return list(qs)


@register_view(views)
def get_years(request, token):
    """ Returns list of integer years, it reads only token, doesn't check DB """
    if request.method != "GET":
        return HttpResponseNotAllowed()

    return JsonResponse(_get_years_from_token(token), safe=False)


@register_view(views)
class GetDataByYearView(APIView):
    safe_json = False
    error_on_empty_data = True  # TODO: separate empty data and bad requests

    def get_data(self):
        year = self.kwargs['year']
        units = self.request.GET.get('unitId', DEFAULT_UNITS)
        try:
            power10 = get_factor_from_units(units)
        except (IndexError, ValueError):
            self.error_response = {
                "success": False,
                "error": {"message": f"couldn't parse units: '{units}'",
                          "code": "bad_units"},
                "status_code": 400,
            }
            return False  # tell view to return error response

        try:
            year_entry = StatYearEntry.objects.get(year=year)
        except StatYearEntry.DoesNotExist:
            self.error_response = {
                "success": False,
                "error": {"message": f"no data for year {year}", "code": "no_data"},
                "year": year,
                "status_code": 400,
            }
            return False  # tell view to return error response

        serializer = APIDataByYearSerializer(year_entry, context={'power10': power10,
                                                                  'decimal_places': 2})
        return serializer.data


@register_view(views)
class GetDataSeriesByCategoryView(CachingAPIView):
    """ Returns list of values by years for given categories
        Reqires 'token' and 'categoryId' in self.kwargs
        Fills missing year data with zero values
    """
    safe_json = False
    check_kwargs = ('token',)  # categoryId changed to int converter

    def get_data(self, **kwargs):

        token = self.kwargs['token']
        years = _get_years_from_token(token)
        units = _get_units_from_token(token)
        power10 = get_factor_from_units(units)

        # vqs == Values QuerySet (objects.values())
        vqs = SNACategory.objects.api_values_by_category(
            self.kwargs['categoryId'],
            years=years,
            power10=power10,
            decimal_places=0
        )

        year_data = dict.fromkeys(years, 0)
        for item_dict in vqs:
            year_data[item_dict["year"]] = item_dict["value"]

        return [year_data[year] for year in years]


@register_view(views)
def upload_file(request):
    if request.method == "POST":
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES["docfile"]
            str = file.read()
            # js = json.loads(str)
            print(str)
    else:
        form = UploadFileForm()
    return None  # render(request, "sna/upload.html", {"form": form})


def get_factor_from_units(unit_code, raise_error=True):
    power10 = 6  # default value for silent fallback
    try:
        power10 = int(unit_code[4:])
    except IndexError as ex:
        logger.error(f"Get factor from units got index error: {ex}, unit code was '{unit_code}'")
        if raise_error:
            raise ex
    except ValueError as ex:
        logger.error(f"Get factor from units couldn't cast to int: {ex}, unit code was '{unit_code}'")
        if raise_error:
            raise ex
    return power10


def _get_years_from_token(token):
    return [int(val) for val in token.split(";")[1:]]


def _get_units_from_token(token):
    units = token.split(";")[0]
    # NOTE let views check the unit values
    if units != DEFAULT_UNITS:
        logger.warning(f'Got non-default units in token: {units}')
        # raise ValueError
    return units
