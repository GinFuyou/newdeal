from django.core.management.base import BaseCommand  # , CommandError
from django.db import connection  # , transaction

from sna.models import (SNACategory, StatYearEntry, YearEntryRecord)  # noqa: F401


class Command(BaseCommand):
    help = 'create a testing-purposes GDP instances'

    """
    def add_arguments(self, parser):
        parser.add_argument('poll_ids', nargs='+', type=int)
    """

    def present(self, records, options=None):
        counter = 0
        yearly_table = []

        year_template = "{value: >10}|"
        full_template = "{counter: >3}|{cat_id: >3}|{path: <16}" \
                        "|{name_en: >24}|{rank: >4}|" + year_template

        header = full_template.format(value=records[0].year_entry.year,
                                      counter='#',
                                      cat_id='cid',
                                      name_en='category',
                                      path='path',
                                      rank='rank',)
        separator = full_template.format(value=10 * '-',
                                         path=16 * '-',
                                         counter=3 * '-',
                                         cat_id=3 * '-',
                                         name_en=24 * '-',
                                         rank=4 * '-')

        last_year = records[0].year_entry.year

        cursor = 0
        flag = False
        for record in records:
            if record.year_entry.year != last_year:
                last_year = record.year_entry.year
                flag = True
                cursor = 0
                header += year_template.format(value=last_year)
                separator += (10 * '-') + '+'
            if flag:
                # append to full template a single column
                yearly_table[cursor] += year_template.format(value=record.value)
            else:
                yearly_table.append(full_template.format(value=record.value,
                                                         cat_id=record.category_id,
                                                         counter=counter,
                                                         path=repr(record.category.path_node.lineage),
                                                         name_en=record.category.name_en,
                                                         rank=record.category.rank,
                                                         ))
            counter += 1
            cursor += 1

        separator = separator.replace('|', '+')
        self.stdout.write(separator)
        self.stdout.write(header)
        self.stdout.write(separator)
        for msg in yearly_table:
            self.stdout.write(msg)
        self.stdout.write(separator)

    def get_base_queryset(self, options=[]):
        qs = YearEntryRecord.objects.all().select_related('category',
                                                          'category__path_node',
                                                          'year_entry')
        return qs

    def handle(self, *args, **options):
        self.stdout.write('* start testing fetches by category')

        qs = self.get_base_queryset().order_by('year_entry__year',
                                               'category__parent__rank',
                                               'category__rank')
        self.present(qs)

        qs = self.get_base_queryset()
        qs = qs.order_by('year_entry__year', 'category__path_node__lineage')
        self.present(qs)

        self.present(qs.order_by('year_entry__year',
                                 'category__parent__path_node__lineage',
                                 'category__rank'))

        self.stdout.write('\n ** Queries: ')

        counter = 0
        for query in connection.queries:
            counter += 1
            self.stdout.write(f"[{counter:0>2}]\n{query['time']} ms.\n{query['sql']}\n ----")
