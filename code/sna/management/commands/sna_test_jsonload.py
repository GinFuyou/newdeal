import json
import os
from collections import OrderedDict
from time import time

from django.conf import settings
from django.core.management.base import BaseCommand  # , CommandError
from django.db import connection  # , transaction

from sna import models as sna_models
from sna import serializers as sna_serializers

# from rest_framework.parsers import JSONParser, FileUploadParser


class Command(BaseCommand):
    help = 'test loading and parsing data from data-files, use options to specify data'

    DATA_PATH = settings.DATA_PATH

    def add_arguments(self, parser):
        parser.add_argument('-c','--categories', nargs='?', type=str,
                            const='categories.json',
                            default='',
                            metavar='SUBPATH',
                            help=f'load categories from this subpath in {self.DATA_PATH}')

        parser.add_argument('-r','--records', nargs='?', type=str,
                            const='data.json',
                            default='',
                            metavar='SUBPATH',
                            help=f'load records from this subpath in {self.DATA_PATH}')

        parser.add_argument('-s','--save',
                            action='store_true',
                            help=f'save instances (default is False)')

        parser.add_argument('-q','--queries',
                            action='store_true',
                            help=f'list connection.queries')

        parser.add_argument('--power10',
                            type=int,
                            default=6,
                            help='power of ten for numbers in data to be loaded. e.g. if records are millions of'
                                 ' rouble, power of ten is 6 (10^6 == millions, 10^3 == thousands, etc.)')

    def show_timers(self, timers, hide_zero=True):
        # TODO: implement zero-filtering
        separator = f"+{'-'*59}+"
        self.stdout.write(separator)
        self.stdout.write(f'| {"timers:": <57} |')
        self.stdout.write(separator)
        for key, value in timers.items():
            if (hide_zero and value) or not hide_zero:
                self.stdout.write(f'| {key:.<47}{-1*value:.>8.3f} s.|')
        self.stdout.write(separator)

    def show_queries(self):
        total_time = 0.0
        count = 0
        self.stdout.write("\n Queries: ")
        for query in connection.queries:
            count += 1
            total_time += float(query['time'])
            self.stdout.write(f"[{count:0>2}] {query['time']} s.\n{query['sql']}\n\n")
        self.stdout.write(f"Total {count} queries, {total_time:.3f} s.")

    def handle(self, *args, **options):
        self.stdout.write(f'* test-load JSON from "{self.DATA_PATH}"')
        timers = OrderedDict([
            ('categories_parse_file', 0.0),
            ('categories_creation', 0.0),
            ('records_parse_file', 0.0),
            ('records_creation', 0.0),
        ])

        if options['categories']:
            full_path = os.path.join(self.DATA_PATH, options['categories'])
            self.stdout.write(f' - loading categories from "{full_path}"...')

            if os.path.isfile(full_path):
                # read JSON from file
                timers['categories_parse_file'] = time()
                with open(full_path) as data_file:
                    data = json.load(data_file)
                timers['categories_parse_file'] -= time()

                serializer = sna_serializers.FileImportCategorySerializer(data=data)
                if serializer.is_valid():
                    self.stdout.write(f'   serializer validation is successful')

                    # write objects to database
                    if options['save']:
                        self.stdout.write(f'   saving...')
                        timers['categories_creation'] = time()
                        serializer.save()
                        timers['categories_creation'] -= time()
                else:
                    self.stderr.write(f'   serializer validation failed')
                    for key, items in serializer.errors.items():
                        self.stderr.write(f'   ["{key}"]')
                        for error in items:
                            self.stderr.write(f'   - {error}')
                    self.stderr.write(repr(serializer.errors))

            else:
                self.stderr.write(f' ! ERROR "{full_path}" is not a file or does not exists')

        if options['records']:
            full_path = os.path.join(self.DATA_PATH, options['records'])
            self.stdout.write(f' - test records serialization')

            """
            qs = sna_models.StatYearEntry.objects.filter(year__in=(2014, 2016))[:3]
            serializer = sna_serializers.SimpleYearlyRecordsSerializer(qs, many=True)
            """

            try:
                inst = sna_models.StatYearEntry.objects.get(year=2016)
                serializer = sna_serializers.APIDataByYearSerializer(inst, context={'power10': options['power10']})
                self.stdout.write(f'   read data with {serializer.__class__}:'
                                  f'\n   {repr(serializer.data)}')
            except sna_models.StatYearEntry.DoesNotExist:
                self.stdout.write('   no StatYearEntry data for test read')

            # load data for records
            self.stdout.write(f' - loading records from "{full_path}"...')
            if os.path.isfile(full_path):
                # read JSON from file
                timers['records_parse_file'] = time()
                with open(full_path) as data_file:
                    data = json.load(data_file)
                timers['records_parse_file'] -= time()

                timers['records_serializer_init_and_validate'] = time()
                serializer = sna_serializers.SimpleYearlyRecordsSerializer(data=data, many=True)
                valid = serializer.is_valid()
                timers['records_serializer_init_and_validate'] -= time()

                if valid:
                    self.stdout.write(f'   serializer validation is successful')
                    if options['save']:
                        self.stdout.write(f'   saving...')
                        timers['records_creation'] = time()
                        serializer.save()
                        timers['records_creation'] -= time()
                else:
                    self.stderr.write(f'   serializer validation failed')
                    zero_counter = 0
                    for obj in serializer.errors:
                        self.stderr.write(f'   [{zero_counter}] item (counting from zero)')
                        for key, items in obj.items():
                            self.stderr.write(f'   [{zero_counter}]["{key}"]')
                            for error in items:
                                self.stderr.write(f'    - {error}')
                        zero_counter += 1
                    self.stderr.write(repr(serializer.errors))

            else:
                self.stderr.write(f' ! ERROR "{full_path}" is not a file or does not exists')

        if options['queries']:
            self.show_queries()

        self.show_timers(timers)
        self.stdout.write(" # Finished")
