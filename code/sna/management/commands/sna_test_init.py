from random import randint

from django.conf import settings
from django.core.management.base import BaseCommand  # , CommandError
from django.db import transaction

from sna.models import (SNACategory, SNACategoryTranslation, StatYearEntry,
                        YearEntryRecord)


class Command(BaseCommand):
    help = 'create a testing-purposes GDP instances'

    def add_arguments(self, parser):
        parser.add_argument('--reset', action='store_true', help='delete all categories first')

    def treebeard_init(self):
        """ used to init treeabeard items. not needed otherwise """
        cat_root = SNACategory(name_en='GDP')
        SNACategory.add_root(instance=cat_root)
        cat_root.refresh_from_db()
        cat_consumption = SNACategory(name_en='Cosumption', parent=cat_root)
        cat_root.add_child(instance=cat_consumption)
        cat_consumption.refresh_from_db()

        cat_investment = SNACategory(name_en='Investments', parent=cat_root)
        cat_root.add_child(instance=cat_investment)
        cat_investment.refresh_from_db()

        cat_government = SNACategory(name_en='Government', parent=cat_root)
        cat_root.add_child(instance=cat_government)
        cat_government.refresh_from_db()

        # cat_root.add_child(instance=cat_consumption)
        # cat_root.add_child(instance=cat_investment)
        # cat_root.add_child(instance=cat_government)

        cat_goods = SNACategory(name_en='goods', parent=cat_consumption)

        cat_consumption.add_child(instance=cat_goods)
        cat_goods.refresh_from_db()

        cat_goods.add_child(**{'name_en': 'long-usage goods', 'parent': cat_goods})
        cat_goods.add_child(instance=SNACategory(name_en='half-long usable', parent=cat_goods))
        cat_goods.add_child(instance=SNACategory(name_en='short usable', parent=cat_goods))
        cat_government.add_child(name_en='gov. consumption', parent=cat_government)
        cat_government.add_child(instance=SNACategory(name_en='gov. Investments', parent=cat_government))
        cat_investment.add_child(instance=SNACategory(name_en='prvt. investemnts A', parent=cat_investment))
        cat_investment.add_child(instance=SNACategory(name_en='prvt. investemnts B', parent=cat_investment))
        cat_investment.add_child(instance=SNACategory(name_en='prvt. investemnts C', parent=cat_investment))
        cat_investment.add_child(instance=SNACategory(name_en='prvt. investemnts D', parent=cat_investment))
        cat_root.add_child(instance=SNACategory(name_en='NX Pure export', parent=cat_root))
        cat_consumption.add_child(instance=SNACategory(name_en='services', parent=cat_consumption))
        cat_investment.add_child(instance=SNACategory(name_en='prvt. investemnts F', parent=cat_investment))
        cat_investment.add_child(instance=SNACategory(name_en='prvt. investemnts E',
                                                      parent=cat_investment,
                                                      rank=255))

    def classic_init(self):
        """ init categories as 'classic' plain models. don't use bulk_create()
            because it will cause errors on default priority
        """
        cat_root = SNACategory.objects.create(name_en='GDP', code='fake0000')
        cat_consumption = SNACategory.objects.create(name_en='Cosumption',
                                                     parent=cat_root,
                                                     code='fake0001')
        cat_investment = SNACategory.objects.create(name_en='Investments',
                                                    parent=cat_root,
                                                    code='fake0002')
        cat_government = SNACategory.objects.create(name_en='Government',
                                                    parent=cat_root,
                                                    code='fake0003')

        cat_goods = SNACategory.objects.create(name_en='goods',
                                               parent=cat_consumption,
                                               code='fake0004')

        SNACategory.objects.create(name_en='long usable', parent=cat_goods, code='fake0005')
        SNACategory.objects.create(name_en='half-long usable', parent=cat_goods, code='fake0006')
        SNACategory.objects.create(name_en='short usable', parent=cat_goods, code='fake0007')
        SNACategory.objects.create(name_en='gov. consumption', parent=cat_government, code='fake0008')
        SNACategory.objects.create(name_en='gov. Investments', parent=cat_government, code='fake0009')
        SNACategory.objects.create(name_en='prvt. investemnts A', parent=cat_investment, code='fake0010')
        SNACategory.objects.create(name_en='prvt. investemnts B', parent=cat_investment, code='fake0011')
        SNACategory.objects.create(name_en='prvt. investemnts C', parent=cat_investment, code='fake0012')
        SNACategory.objects.create(name_en='prvt. investemnts D', parent=cat_investment, code='fake0013')
        SNACategory.objects.create(name_en='NX Pure export', parent=cat_root, code='fake0014')
        SNACategory.objects.create(name_en='services', parent=cat_consumption, code='fake0015')
        SNACategory.objects.create(name_en='prvt. investemnts F', parent=cat_investment, code='fake0016')
        SNACategory.objects.create(name_en='prvt. investemnts E', parent=cat_investment, code='fake0017')

    def handle(self, *args, **options):
        """ run the command logic here """

        self.stdout.write(f"! {SNACategoryTranslation.__class__.__name__} not added")
        if options['reset']:
            self.stdout.write(" >> deleting all items")
            if settings.DEBUG:
                YearEntryRecord.objects.all().delete()
                StatYearEntry.objects.all().delete()
                SNACategory.objects.all().delete()
            else:
                self.stdout.write(" !! not in Debug mode, action prevented to avoid data loss")
            return

        self.stdout.write(" >> creating SNACategories")

        with transaction.atomic():
            self.classic_init()

            self.stdout.write(" >> creating StatYearEntries")
            y2011 = StatYearEntry.objects.create(year=2011)
            y2012 = StatYearEntry.objects.create(year=2012)
            y2013 = StatYearEntry.objects.create(year=2013)
            y2014 = StatYearEntry.objects.create(year=2014)
            y2015 = StatYearEntry.objects.create(year=2015)

            self.stdout.write(" >> creating YearEntryRecords")
            for year_entry in [y2011, y2012, y2013, y2014, y2015]:
                record_list = []
                for category in SNACategory.objects.all():
                    if year_entry.year == 2011:
                        value = randint(1000, 5000000)
                    else:
                        previous_record = YearEntryRecord.objects.get(
                            year_entry__year=year_entry.year - 1,
                            category=category)
                        value = previous_record.value * 1.1

                    r = YearEntryRecord(year_entry=year_entry,
                                        value=value,
                                        category=category)
                    r.full_clean()
                    record_list.append(r)
                YearEntryRecord.objects.bulk_create(record_list)
        self.stdout.write("done")
