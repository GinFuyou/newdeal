from django.contrib import admin
from django.db.models import F

from . import models as sna_models


@admin.register(sna_models.SNACategory)
class SNACategoryAdmin(admin.ModelAdmin):
    list_display = ('name_en',
                    'code',
                    'parent',
                    'update_datetime',
                    'get_level',
                    'pk',
                    'get_lineage')
    search_fields = ('name_en', 'code')
    ordering = ('path_node__lineage',)

    def get_lineage(self, obj):
        return repr(obj.lineage)
    get_lineage.short_description = "tree lineage"

    def get_level(self, obj):
        return obj.path_node.level
    get_level.short_description = "level"

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.select_related('path_node')
        qs = qs.annotate(lineage=F('path_node__lineage'))
        return qs


@admin.register(sna_models.SNACategoryTranslation)
class SNACategoryTranslationAdmin(admin.ModelAdmin):
    list_display = (str, 'get_lang_display', 'category', 'update_datetime')
    search_fields = ('name',)
    list_filter = ('lang',)


@admin.register(sna_models.StatYearEntry)
class StatYearEntryAdmin(admin.ModelAdmin):
    list_display = ('year',)


@admin.register(sna_models.YearEntryRecord)
class YearEntryRecordAdmin(admin.ModelAdmin):
    list_display = (str, 'year_entry', 'int_value', 'percent', 'update_datetime')
    list_filter = ('year_entry',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.with_category_tree()
        return qs
