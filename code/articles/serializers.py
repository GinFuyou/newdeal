from rest_framework import serializers

from articles.models import Article


class ArticleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = ('title', 'body', 'pin_rank', 'newsindex', 'live', 'date')

    def create(self, validated_data):
        return Article.objects.create(**validated_data)
