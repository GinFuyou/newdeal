from datetime import timedelta

from django.conf import settings
from django.db import models
from django.db.models.constraints import CheckConstraint
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import TaggedItemBase
from transliterate import translit
from wagtail.admin.edit_handlers import FieldPanel, HelpPanel
from wagtail.contrib.routable_page.models import route
from wagtail.core.fields import RichTextField
from wagtail.core.models import Page
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index
from wagtail.search.backends import get_search_backend

from home.model_base import AbstractDateTimeTrackedModel
from wagtailnews.decorators import newsindex
from wagtailnews.models import (AbstractNewsItem, AbstractNewsItemRevision,
                                NewsIndexMixin, NewsItemQuerySet)


# The decorator registers this model as a news index
@newsindex
class ArticleIndex(NewsIndexMixin, Page):
    # Add extra fields here, as in a normal Wagtail Page class, if required
    newsitem_model = 'Article'

    @route(r'^search/$', name='search')
    def search(self, request):
        """ Search articles by GET params """
        search_query = request.GET.get('q', None)
        if search_query:
            searcher = get_search_backend()
            results = searcher.search(search_query,
                                      self.get_newsitems_for_display(),
                                      partial_match=True)
        else:
            results = self.get_newsitems_for_display().none()

        return self.respond(request, 'all', results, {'search_query': search_query})

    def paginate_newsitems(self, request, newsitem_list):
        """ I will extend pagination context name for universal pagination """
        context = super().paginate_newsitems(request, newsitem_list)
        context.update({'page_obj': context['newsitem_page']})
        return context

    def get_context(self, request, view, **kwargs):
        context = super().get_context(request, view, **kwargs)
        context.update({'search_url': f"{self.get_url(request)}search/"})  # TODO dynamic reversing
        return context

    def get_newsitems_for_display(self):
        """
        Get the news items that should be shown on for this news index, before
        filtering and pagination. It will implement ordering over default Wagtailnewsordering
        """
        queryset = self.get_newsitems().live_ordered()
        # print(queryset.query)
        return queryset


class ArticleQuerySet(NewsItemQuerySet):
    def live_ordered(self, order_by=('-pin_rank', '-date')):  # TODO: replace with update_datetime for prod
        qs = self.live().filter(date__lte=timezone.now())
        return qs.order_by(*order_by)

    def for_homepage(self):
        """ implement your own logic for articles to be shown on home page """
        articles = models.Q(newsindex__slug='articles', date__gte=timezone.now() - timedelta(days=21))
        news = models.Q(newsindex__slug='news')
        qs = self.live_ordered().filter(articles | news)
        return qs


class Article(AbstractNewsItem, AbstractDateTimeTrackedModel):
    # NewsItem is a normal Django model, *not* a Wagtail Page.
    # Add any fields required for your page.
    # It already has ``date`` field, and a link to its parent ``NewsIndex`` Page

    class Meta(AbstractNewsItem.Meta):
        verbose_name = _('article')
        ordering = ()

        # Database-level constraints
        constraints = [
            CheckConstraint(check=~models.Q(title=''), name='%(app_label)s_check_title_emptystr',),
        ]

    title = models.CharField(_('title'), max_length=255)
    body = RichTextField(_('article body'))

    # added to support view count, not yet implemented
    view_count = models.PositiveIntegerField(_("view count"), default=0)

    # used to pin articles above others, default 0 is "sort as usual by date"
    pin_rank = models.PositiveSmallIntegerField(_("pinning rank"), default=0)
    pin_rank.help_text = _('0 == article is ordered by date as usual, bigger value makes article'
                           ' pinned higher than others')

    # see help_text
    cover_image = models.ForeignKey('wagtailimages.Image',
                                    verbose_name=_('cover image'),
                                    null=True, blank=True,
                                    on_delete=models.SET_NULL,
                                    related_name='covers_articles')
    cover_image.help_text = _("Image to show as article cover, above title and as preview in"
                              " the list of articles.")

    # NOTE: instead of category model will be using different Wagtailnews Index page (see above)
    # category = models.ForeignKey("ArticleCategory", blank=True, null=True,
    #   on_delete=models.PROTECT)

    tags = ClusterTaggableManager(through='ArticleTag', blank=True, verbose_name=_('tags'))

    panels = [
        FieldPanel('title', classname='full title'),
        HelpPanel('<b>Закрепление статьи</b>: установите значение выше 0, чтобы закрепить статью'
                  ' над остальными.<br /> Статьи сначала сортируются по рангу от большего к меньшему,'
                  ' потом по дате', ),  # TODO change to translation string
        FieldPanel('pin_rank', classname='full'),
        ImageChooserPanel('cover_image'),
        FieldPanel('body', classname='full'),
        FieldPanel('tags'),
    ] + AbstractNewsItem.panels

    search_fields = [
        index.SearchField('body', partial_match=True),
        index.SearchField('title', partial_match=True),
        index.FilterField('newsindex_id'),
        index.FilterField('live'),
        index.FilterField('date'),
    ]

    objects = ArticleQuerySet.as_manager()

    def __str__(self):
        return self.title

    def get_smart_trunc_words_count(self):
        """ smartly truncate words based on length of title with static minimum """
        BASE_WORDS = 80
        TITLE_LIMIT = 36
        title_len = len(self.title)
        result = BASE_WORDS
        if title_len > TITLE_LIMIT:
            result = max(BASE_WORDS - (title_len - TITLE_LIMIT) * 2, 32)
        return result

    def get_slug(self):
        # by default - dissallow unicode in slugs
        allow_unicode = getattr(settings, 'WAGTAIL_ALLOW_UNICODE_SLUGS', False)
        text = self.title

        # if unicode is not allowed, translit cyrillics so it's not stripped
        if not allow_unicode:
            text = translit(text, 'ru', reversed=True)
        return slugify(text, allow_unicode=allow_unicode)

    def serve(self, request):
        """ update view count before serving """
        if not request.user.is_staff:
            article_pk_list = request.session.get('viewed_articles', [])
            if self.pk not in article_pk_list:
                self.view_count = models.F('view_count') + 1
                self.save(update_fields=['view_count'])
                article_pk_list.append(self.pk)
                request.session['viewed_articles'] = sorted(article_pk_list)
                request.session.modified = True
                # NOTE remove debug prints
                # print(f"view count updated: {self.view_count} #{self.pk}")
            else:
                pass
                # NOTE remove debug prints
                # print(f"viewed articles list: {repr(article_pk_list)}")
        return super().serve(request)


class ArticleRevision(AbstractNewsItemRevision):

    class Meta:
        verbose_name = _('article revision')

    newsitem = models.ForeignKey(Article, related_name='revisions', on_delete=models.CASCADE)

    # this field duplicates built-in "date" field which is confusing, because it
    # represents datatime object
    # use this instead writting new code.
    create_datetime = models.DateTimeField(_('create date and time'), default=timezone.now)


class ArticleTag(TaggedItemBase):
    """ Tag through model for Aricle tags. Made according to Wagtail docs. """
    content_object = ParentalKey('Article',
                                 related_name='tagged_items',
                                 on_delete=models.CASCADE)
