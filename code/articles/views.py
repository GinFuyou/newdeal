from django.utils import timezone
from django.views.generic.list import ListView
from wagtail.search.backends import get_search_backend

from articles.models import Article


class SearchAllView(ListView):
    """ search on all news indeces """
    model = Article
    template_name = 'articles/article_index.html'
    ordering = ('-date',)  # TODO perfectly change the field
    search_query = None
    paginate_by = 20  # TODO global pagination settings

    def get_queryset(self):
        """ get all article and apply search """
        queryset = self.model.objects.live().filter(date__lte=timezone.now())

        self.search_query = self.request.GET.get('q', None)
        if self.search_query:
            print(f'searching... {self.search_query}')
            # we don't need ordering if there is nothing to search
            ordering = self.get_ordering()
            if ordering:
                queryset = queryset.order_by(*ordering)

            searcher = get_search_backend()
            results = searcher.search(self.search_query,
                                      queryset,
                                      partial_match=True)
            print(results)
        else:
            results = queryset.none()

        return results

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'newsitem_list': context['object_list'],
                        'search_query': self.search_query,
                        # This setting is needed to tweak template behaviour
                        'search_all': True})
        return context


search_all = SearchAllView.as_view()
