# Установка 
### Предварительные зависимости
Установлены docker и docker-compose

### Разворачивание сайта для разработки

 #### 1. Клонируйте с помощью *git* (или распакуйте архив)

 ```
 git clone git@gitlab.com:GinFuyou/newdeal.git
 ```
 перейдите в папку проекта

 ```
 cd newdeal/
 ```

 #### 2. Соберите окружение для dev
 ```
 sudo docker-compose build
 ```

 #### 3. Запустите проект для dev
 ```
 sudo docker-compose up
 ```

 #### 4. Проверьте работоспособность проекта

 Откройте браузер и перейдите на адрес *http://localhost:8000* 

 #### 5. Для сборки для production
 ```
 sudo build.prod.sh
 ```

 #### 6. Для запуска для production
 ```
 sudo run.prod.sh
 ```

### Обновление сайта на тестовом сервере (http://kate2.doratoa.net:8000/)
#### 1. Получить креды для подключения по SSH у @GinFuyou
#### 2. Подключиться по SSH
#### 3. Перейти в папку проекта
```
cd newdeal
```
#### 4. Проверить текущий репозторий
```
git status
```
Получаем чтото типа:
```
git status
On branch master
Your branch is up to date with 'origin/master'.
```
Должна быть выбрана ветка **master**

#### 5. Забрать послединие изменения из gitlab
```
git pull
```
#### 6. Собрать новые образы
```
sudo /home/newdeal/newdeal/build.prod.sh
```

#### 7. После успешной сборки запустить контейнеры из новых образов
```
sudo /home/newdeal/newdeal/run.prod.sh
```

#### 8. После того как логах не видим ошибок - отключаемся сочетанием Ctrl + Z
