#!/bin/sh

set -e
set -x

echo "$SCHEDULE cd / && /backup.sh" >> cron.cfg
crontab cron.cfg
rm cron.cfg
/usr/sbin/crond -f -l 8