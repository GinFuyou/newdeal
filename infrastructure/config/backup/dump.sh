#!/bin/sh

set -e
set -o pipefail

# Verify POSTGRES_* parameters
if [ "${POSTGRES_HOST}" = "**None**" ]; then
  echo "You need to set the POSTGRES_HOST environment variable."
  exit 1
fi

if [ "${POSTGRES_DATABASE}" = "**None**" ]; then
  echo "You need to set the POSTGRES_DATABASE environment variable."
  exit 1
fi

if [ "${POSTGRES_USER}" = "**None**" ]; then
  echo "You need to set the POSTGRES_USER environment variable."
  exit 1
fi

if [ "${POSTGRES_PASSWORD}" = "**None**" ]; then
  echo "You need to set the POSTGRES_PASSWORD environment variable."
  exit 1
fi

if [ "${MEDIA_DIR}" = "**None**" ]; then
  echo "You need to set the MEDIA_DIR environment variable."
  exit 1
fi

# Set parameters for dump
export PGPASSWORD=$POSTGRES_PASSWORD
POSTGRES_HOST_OPTS="-h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USER"

# Dump the database
echo "Creating dump of database ${POSTGRES_DATABASE} from host ${POSTGRES_HOST}..."
pg_dump $POSTGRES_HOST_OPTS $POSTGRES_EXTRA_OPTS ${POSTGRES_DATABASE} --file=/tmp/db.dmp
echo "Creating dump of media ${MEDIA_DIR}..."
tar -cvf $BACKUP_FILE /tmp/db.dmp $MEDIA_DIR
rm /tmp/db.dmp




